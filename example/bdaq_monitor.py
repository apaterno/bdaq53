''' This example explains how to use the online_monitor with bdaq53

    The online_monitor python package consists of independent
    processes for data analysis and visualization. It cannot
    cause the data taking to crash (by design) and data analysis can
    happen on other PC(s). If the analysis is too demanding data
    is dropped, in favour of staying real-time.

    Online_monitor has a plug-in system for multiple DAQs.
    The plugins for bdaq53 are in bdaq53/analysis/online_monitor

    One yaml file describes the setup. An example yaml is given in this
    example folder (online_monitor.yaml).

    To run the online monitor for this example open a console,
    goto this example folder and type:
    start_online_monitor online_monitor.yaml

    This starts the online_monitor with bdaq53 simulation, bdaq53 converter
    and bdaq53 receiver.

    One can also run this example to start the online monitor.

    A simulation of bdaq53 data taking is used (data is replayed), hardware
    is not needed. You have to define the data to be replayed in the .yaml
    file. E.g. use the threshold scan unit test data
    (tests/fixtures/threshold_scan.h5)
    If you want to slow down the replay add a delay in seconds into the yaml.

    Replaying data is useful to spot time related effects.
'''

import subprocess
import os

from online_monitor.utils import settings

import bdaq53


def run_script_in_shell(script, arguments, command=None):
    if os.name == 'nt':
        creationflags = subprocess.CREATE_NEW_PROCESS_GROUP
    else:
        creationflags = 0
    return subprocess.Popen("%s %s %s" % ('python' if not command else command,
                                          script, arguments), shell=True,
                            creationflags=creationflags)


if __name__ == '__main__':
    # Get the absoulte path of the online_monitor installation
    package_path = os.path.dirname(bdaq53.__file__)
    # Add online_monitor plugin folder to entity search paths
    settings.add_producer_sim_path(os.path.join(package_path,
                                                'analysis',
                                                'online_monitor'))
    settings.add_converter_path(os.path.join(package_path,
                                             'analysis',
                                             'online_monitor'))
    settings.add_receiver_path(os.path.join(package_path,
                                            'analysis',
                                            'online_monitor'))
    # Start online_monitor
    run_script_in_shell('', 'online_monitor.yaml', 'start_online_monitor')
