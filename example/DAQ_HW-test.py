# ------------------------------------------------------------
# BDAQ53: Simple hardware test
# Basic DAQ hardware and chip configuration
#
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import os
import time
import numpy as np
import logging
import yaml
from basil.dut import Dut
from bdaq53.rd53a import rd53a
from basil.HL.GPAC import GpioPca9554
import bdaq53


activelanes_tx = 1
activelanes_rx = activelanes_tx

timeout = 10



#@unittest.skip('Needs to be rewritten')
class DAQ_HW_Test(unittest.TestCase):
    def setUp(self):
        proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) #../
        top_dir = os.path.dirname(os.path.dirname(proj_dir))
        xilinx_dir = os.environ.get('XILINX_VIVADO')

        with open(proj_dir + '/bdaq53/bdaq53.yaml', 'r') as f:
            cnfg = yaml.load(f)

        self.chip = rd53a(cnfg)
        self.chip.init()

        gpio_expander_A0_conf = {'name' : 'GpioPca9554', 'type' : 'GpioPca9554', 'interface': 'intf', 'base_addr': 0x00}
        gpio_expander_A0 = GpioPca9554(self.chip['i2c'], gpio_expander_A0_conf)
        logging.info('IO expander #0 input port: %s' % gpio_expander_A0._read_input_port() )
        logging.info('IO expander #0 output port: %s' % gpio_expander_A0._read_output_port() )

        gpio_expander_A1_conf = {'name' : 'GpioPca9554', 'type' : 'GpioPca9554', 'interface': 'intf', 'base_addr': 0x01}
        gpio_expander_A1 = GpioPca9554(self.chip['i2c'], gpio_expander_A1_conf)
        logging.info('IO expander #1 input port: %s' % gpio_expander_A1._read_input_port() )
        logging.info('IO expander #1 output port: %s' % gpio_expander_A1._read_output_port() )

        gpio_expander_A2_conf = {'name' : 'GpioPca9554', 'type' : 'GpioPca9554', 'interface': 'intf', 'base_addr': 0x02}
        gpio_expander_A2 = GpioPca9554(self.chip['i2c'], gpio_expander_A2_conf)
        logging.info('IO expander #2 input port: %s' % gpio_expander_A2._read_input_port() )
        logging.info('IO expander #2 output port: %s' % gpio_expander_A2._read_output_port() )


    def test(self):
        # Configure the CMD encoder
        self.chip['cmd'].reset()
        self.chip['cmd'].start()
        logging.info("Mem size: %u", self.chip['cmd'].get_mem_size())

        # Configure the RD53A chip: Set lanes and CB parameters
        self.chip.set_aurora(tx_lanes = activelanes_tx, CB_Wait = 255, CB_Send = 1, chip_id = 8, write = True)
        self.chip.write_ecr(write = True)

        # Set Aurora reference clock sources
        self.chip['rx'].set_mgt_ref("int")
        logging.info("Ref mode:" + self.chip['rx'].get_mgt_ref())
        self.chip['rx'].reset()
        time.sleep(0.1)

        # Aurora ready?
        logging.info("Wait for Aurora rx_ready")
        for _ in range(3):
            if self.chip['rx'].get_rx_ready():
                logging.info("Aurora RX_CHANNEL_UP!")
                break
            time.sleep(1)
            assert self.chip['rx'].get_rx_ready() == 1


    def tearDown(self):
        self.chip.close()  # close connection and stop simulator


if __name__ == '__main__':
    unittest.main()
