#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import os
import numpy as np
import sys
import yaml
import logging
import argparse

import utils
from basil.utils.sim.utils import cocotb_compile_and_run, cocotb_compile_clean
from bdaq53.rd53a import rd53a


activelanes_rx = 1
activelanes_tx = activelanes_rx
EnableMonitorData = False

rd53a_size = True

cmd_args_c = 20
cmd_args_f = 'test_full.csv'


def proc_hit_file(filename, latency=500):
    data_dtype = {'names':['bcid','col','row','tot','trig'], 'formats':['uint32','uint16','uint16', 'uint8','uint8']}
    my_data = np.genfromtxt(filename, delimiter=',', dtype=data_dtype)
    trigger_bx = np.unique(my_data['bcid'][my_data['trig']==1])
    trigger_bx += 22

    hit_data = my_data[my_data['col']<400]
    hit_data['bcid'] += (latency + 2)
    hit_data = np.sort(hit_data)

    hit_data = hit_data[np.in1d(hit_data['bcid'], trigger_bx)]
    hit_data['trig'] = 0
    #TODO: hit merging?

    if rd53a_size:
        hit_data['col'] = hit_data['col']*2
        hit_data['row'] = hit_data['row']+192
        hit_data = hit_data[hit_data['col'] < 50*8]

    return hit_data, trigger_bx


def proc_data(data_in):
    trigger_bx = data_in['bcid'][data_in['multicol']>50]

    data_dtype = {'names':['bcid','col','row','tot','trig'], 'formats':['uint32','uint16','uint16', 'uint8','uint8']}
    hit_data = np.array([], dtype = data_dtype)

    for i in data_in:
        if i['multicol'] < 50:
            col_mod = i['region'] % (96/ (2 if rd53a_size else 1))
            core = col_mod/16
            in_col = 5-core
            reg = col_mod%16
            row = in_col*64+reg*4
            bcid = i['bcid']
            col = i['multicol']*8 + 7 - i['region']/(96/ (2 if rd53a_size else 1))

            for pix_inx, pix_reg in enumerate(['tot0','tot1','tot2','tot3']):
                if i[pix_reg] != 15:
                    tot = i[pix_reg]+1
                    row += pix_inx
                    print i, 'bcid=' ,bcid,'multicol=', i['multicol'], 'region=',i['region'], 'col=', col, 'row=', row, 'core=', core
                    hit_data = np.append(hit_data, np.array([(bcid, col, row, tot, False)], dtype = data_dtype))


    hit_data = np.sort(hit_data)

    return hit_data, trigger_bx

@unittest.skip('Needs to be rewritten using analog_scan...')
class TestHitFile(unittest.TestCase):
    def setUp(self):

        proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) #../
        top_dir = os.path.dirname(os.path.dirname(proj_dir))
        xilinx_dir = os.environ.get('XILINX_VIVADO')
        rd53a_src_dir = os.environ.get('RD53A_SRC')

        logging.info('Using csv file: %s  Test Column: %d\n' % (cmd_args_f, cmd_args_c) )

        #self.hit_file = proj_dir + '/tests/testdata.csv' # /tests/test_full.csv'
        self.hit_file = proj_dir + '/tests/' + cmd_args_f
        self.test_dc = cmd_args_c


        extra_defines = ['INCLUDE_DUT=1', 'INIT_PIXEL_CONF=1','RTL_SIM', 'AURORA_'+str(activelanes_rx)+'LANE']
        if self.test_dc >= 0:
            extra_defines += ['TEST_DC='+str(self.test_dc)]

        cocotb_compile_and_run(

            sim_files = [proj_dir + '/tests/hdl/bdaq53_tb.v', proj_dir + '/firmware/src/bdaq53_core.v'],
            top_level = 'tb',
            extra_defines = extra_defines,
            include_dirs = (proj_dir, proj_dir + "/tests",
                            proj_dir + "/firmware/src",
                            proj_dir + "/firmware/src/rx_aurora",
                            rd53a_src_dir, rd53a_src_dir + "/src/verilog/",
                            xilinx_dir + '/data/verilog/src'),
            extra = 'export SIMULATION_MODULES='+yaml.dump({'drivers.HitDataFile' : {'filename' : self.hit_file}}) +
            '\nVSIM_ARGS += -L ../secureip -L ../unisims  work.glbl \nVHDL_SOURCES+=' + rd53a_src_dir + '/src/verilog/array/cba/regionDigitalWriter.vhd \n'
        )
        #'''
        with open(proj_dir + '/bdaq53/bdaq53.yaml', 'r') as f:
            cnfg = yaml.load(f)

        cnfg['transfer_layer'][0]['type'] = 'SiSim'
        cnfg['hw_drivers'].append({'name': 'fifo', 'type': 'sram_fifo', 'interface' : 'intf', 'base_addr' : 0x8000, 'base_data_addr': 0x80000000})

        self.chip = rd53a(cnfg)
        self.chip.init()


    def test_file(self):
        logging.info('Starting file input test')

        # Configure cmd encoder
        self.chip['cmd'].reset()
        self.chip['cmd'].start()

        # Wait for PLL lock
        self.assertTrue(self.chip.wait_for_pll_lock())

        # Setup Aurora
        self.chip.set_aurora(write=True)
        self.chip.write_ecr(write=True)
        self.assertTrue(self.chip.wait_for_aurora_sync())


        '''TEST USER_K'''
        if EnableMonitorData:
            indata = self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0x0100, write=False)               # Enable USER_K (monitor) data output
            indata += self.chip.write_global_pulse(width=4, write=False)                           # Send global pulse to enable user data
            self.chip.write_command(indata)
            logging.info("Monitor output enabled")

            for regaddr in range(20):
                indata = self.chip.write_sync(write=False)
                indata += self.chip.read_register(register=regaddr, write=False)
                indata += self.chip.write_sync(write=False)
                self.chip.write_command(indata)
        '''TEST USER_K'''


        self.chip['control']['CLK_BX_GATE'] = 1
        self.chip['control'].write()

        logging.info("Enable Columns")
        indata = self.chip.write_sync(write=False)
        indata += self.chip.write_register(register='EN_CORE_COL_SYNC', data=0xffff, write=False) # //  EnableColumn
        indata += self.chip.write_register(register='EN_CORE_COL_LIN_1', data=0xffff, write=False) # //  EnableColumn
        indata += self.chip.write_register(register='EN_CORE_COL_LIN_1', data=0xffff, write=False) # //  EnableColumn
        indata += self.chip.write_register(register='EN_CORE_COL_DIFF_1', data=0xffff, write=False) # //  EnableColumn
        indata += self.chip.write_register(register='EN_CORE_COL_DIFF_2', data=0xffff, write=False) # //  EnableColumn
        indata += self.chip.write_sync(write=False)
        indata += self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0b0100000000000000, write=False)
        indata += self.chip.write_sync(write=False)
        indata += self.chip.write_global_pulse(width = 14, write=False)
        indata += self.chip.write_sync(write=False)
        self.chip.write_command(indata)

        self.chip['cmd'].EXT_TRIGGER_EN = 1
        self.chip.write_bcr(write=True)
        logging.info("Start Hits")

        #wait for data
        rawdata = self.chip['fifo'].get_data()
        prev_size=0
        cnt=0
        for i in range(100):
            rawdata = np.hstack((rawdata, self.chip['fifo'].get_data()))
            size = len(rawdata)
            if i % 10==0:
                logging.info("#%d Fifo size: %d", i, size)

        logging.info("Received: %d words\n", len(rawdata))
        np.save('/tmp/hit_file.npy', rawdata)
        data, userk_data = self.chip.interpret_data(rawdata)

        hit_data_file, trigger_bx_file = proc_hit_file(self.hit_file)
        hit_data_rec, trigger_bx_rec = proc_data(data)

        logging.info('Triggers received: %s' % (str(trigger_bx_rec.size),))
        logging.info('Triggers expected: %s\n' % (str(trigger_bx_file.size),))

        userk_regs = self.chip.proc_userk(userk_data)
        logging.info('USER_K frames received: %s\n' % (str(userk_data.size),))
        logging.info('USER_K registers received: %s\n' % (str(userk_regs.size),))

        if self.test_dc >= 0:
            hit_data_file = hit_data_file[hit_data_file['col'] >= self.test_dc*8]
            hit_data_file = hit_data_file[hit_data_file['col'] < (self.test_dc+1)*8]


        print "--------------FILE--------------"
        print hit_data_file
        print "-----FILE TRIGGERS-----"
        print trigger_bx_file
        print "--------------REC---------------"
        print hit_data_rec
        print "-----REC TRIGGERS-----"
        print trigger_bx_rec
        #print "--------------USER_K------------"
        #print userk_regs
        print "--------------------------------"

        logging.info('Hit pixel received: %s' % (str(hit_data_rec.size),))
        logging.info('Hit pixel expected: %s' % (str(hit_data_file.size),))
        logging.info('USER K frames received: %s' % (str(userk_data.size),))

        diff_trigger  = [x for x in trigger_bx_rec if x not in trigger_bx_file]
        logging.info('Diff Trigger Data: %s' % (str(diff_trigger)))
        diff_hit_data = [x for x in hit_data_rec if x not in hit_data_file]
        logging.info('Diff Hit Data: %s' % (str(diff_hit_data)))

        self.assertEqual(np.array_equal(trigger_bx_file, trigger_bx_rec), True, 'Triggers don\'t match.')
        self.assertEqual(np.array_equal(hit_data_file, hit_data_rec), True, 'Data doesn\'t match.')


    def tearDown(self):
        utils.close_sim(self.chip)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', default='20', type=int, help='Test column number, -1 = all')
    parser.add_argument('-f', default='test_full.csv', help='csv hit file')
    cmd_args = parser.parse_args()
    cmd_args_c = cmd_args.c
    cmd_args_f = cmd_args.f

    del sys.argv[1:]

    unittest.main()
    