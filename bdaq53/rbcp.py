# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import time
import random
from basil.dut import Dut


conf = """
    transfer_layer:
      - name  : intf
        type  : SiTcp
        init:
            ip : "192.168.10.16"
            udp_port : 4660
            tcp_port : 24
            tcp_connection : True

    hw_drivers:
      - name      : gpio
        type      : gpio
        interface : intf
        base_addr : 0x0000
        size      : 8

      - name      : i2c
        type      : i2c
        interface : intf
        base_addr : 0x1000
"""

WRITE_ONLY_IP_AND_MAC = True
ip_addr = [192,168,10,16]
mac_addr= [0x02,0x00,0xC0,0xA8,0x00,0x10]

#chip = Dut("bdaq53.yaml")
chip = Dut(conf)
chip.init()


def write_eeprom(addr, data):
    print ("writing data: %s to address %s" % (data, hex(addr)))
    chip._transfer_layer['intf'].write(addr, data)
    time.sleep(0.1)
    ret = chip._transfer_layer['intf'].read(addr,len(data))
    print ("read back: %s \n" %ret)
    assert(data, ret)


print ("Activating EEPROM write mode\n")
addr = int('0xfffffcff',16)
data = [int('0x00',16)]
write_eeprom(addr, data)


#MAC address
addr = int('0xfffffc12',16)
data = mac_addr
write_eeprom(addr, data)

#IP address
addr = int('0xfffffc18',16)
data = ip_addr
write_eeprom(addr, data)


# In case we want not only to write IP and MAC, write complete default config to the EEPROM
if WRITE_ONLY_IP_AND_MAC == False:
    #Compilation date YY MM DD NN
    addr = int('0xfffffc00',16)
    data = [17,9,6,1]
    write_eeprom(addr, data)

    #SiTCP control
    addr = int('0xfffffc10',16)
    data = [0x01]
    write_eeprom(addr, data)

    #TCP Data port (main)
    addr = int('0xfffffc1c',16)
    data = [0x00, 0x18]
    write_eeprom(addr, data)

    #TCP Command port (alternative)
    addr = int('0xfffffc1e',16)
    data = [0x00, 0x17]
    write_eeprom(addr, data)

    #TCP max segment size
    addr = int('0xfffffc20',16)
    data = [0x05, 0xb4]
    write_eeprom(addr, data)

    #UDP port (RBCP)
    addr = int('0xfffffc22',16)
    data = [0x12, 0x34]
    write_eeprom(addr, data)

    #TCP keep alive timer /ms
    addr = int('0xfffffc24',16)
    data = [0x03, 0xe8]
    write_eeprom(addr, data)

    #TCP keep alive transmit timer /ms
    addr = int('0xfffffc26',16)
    data = [0xea, 0x60]
    write_eeprom(addr, data)

    #TCP time out (open) /ms
    addr = int('0xfffffc28',16)
    data = [0x13, 0x88]
    write_eeprom(addr, data)

    #TCP time out (close) /ms
    addr = int('0xfffffc2a',16)
    data = [0x2b, 0xf2]
    write_eeprom(addr, data)

    #TCP waiting time between connections /ms
    addr = int('0xfffffc2c',16)
    data = [0x01, 0xf4]
    write_eeprom(addr, data)

    #TCP time out for retransmission /ms
    addr = int('0xfffffc2e',16)
    data = [0x01, 0xf4]
    write_eeprom(addr, data)

    #Reserved
    addr = int('0xfffffc30',16)
    data = [0]*16
    write_eeprom(addr, data)
