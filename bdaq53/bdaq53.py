#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import argparse
import os
import logging
import yaml
from importlib import import_module
from inspect import getmembers


def main():
    scan_names = ['scan_digital', 'scan_analog', 'scan_threshold']
    
    parser = argparse.ArgumentParser(
        description='Bonn DAQ system for RD53A prototype\nexample: bdaq53 scan_digital -p start_column=10 stop_column=20', formatter_class=argparse.RawTextHelpFormatter)
     
    parser.add_argument('scan',
                        type=str,
                        choices=scan_names,
                        help='Scan name. Allowed values are:\n' + ', '.join(scan_names),
                        metavar='scan_name')
    
    parser.add_argument('parameter_file',
                        type=str,
                        nargs='?',
                        help='Path to scan parameter file. If none the one from bdaq53/scans/scan_name.yaml is used.',
                        metavar='parameter_file')
    
    parser.add_argument('-p', '--parameters',
                        type=str,
                        nargs='+',
                        help='or list of parameters. E.g. paramA=9 paramB=1',
                        metavar='',
                        default=[])
               
    args = parser.parse_args()

    mod = import_module('bdaq53.scans.'+args.scan)
    
    if args.parameter_file:
        parameter_file = args.parameter_file 
    else:
        parameter_file = os.path.dirname(os.path.abspath(mod.__file__)) + '/' + args.scan + '.yaml'
    
    logging.info('Using parameter file: ' + parameter_file  + '\n')
    
    with open(parameter_file, 'r') as f:
        config = yaml.load(f)
    
    for param in args.parameters:
        key, value = param.split('=')
        config[key] = eval(value)
    
    for _, typ in getmembers(mod):
        if isinstance(typ, type):
            tst = str(typ).split('.')
            if len(tst) == 4 and tst[1] == 'scans':
                cls = typ
    
    scan = cls()
    scan.start(**config)
    scan.analyze()


if __name__ == '__main__':
    main()