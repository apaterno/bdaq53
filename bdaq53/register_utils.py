#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#
import os
import yaml
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)



class RD53ARegisterExtraction():
    '''
    Generates local lookup file in YAML format from Google spreadsheet
    '''

    def __init__(self):
        pass

    def open_worksheet(self, credfile, key, worksheet=0):
        '''
        In order for this to work, your Google accounts needs to have access to the spreadsheet
        and you need to activate the Google sheets API and create a credential file.
        For further information see https://developers.google.com/sheets/api/quickstart/python
        '''

        import gspread
        from oauth2client.service_account import ServiceAccountCredentials

        scope = ['https://spreadsheets.google.com/feeds']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(credfile, scope)
        gc = gspread.authorize(credentials)
        return gc.open_by_key(key).get_worksheet(1)

    def _interpret_number(self, val, size=0):
        val = val.replace("_", "")
        val = val.replace("''", "'")
        if "'" in val:
            _, number = val.split("'")
            if number[0] == 'h':  # Hexadecimal
                out = int(number[1:], 16)
            elif number[0] == 'd':  # Decimal
                out = int(number[1:], 10)
            elif number[0] == 'b':  # Binary
                out = int(number[1:], 2)
            else:
                raise NotImplementedError
        elif val == '0':
            out = 0
        elif val == '1':
            out = 1
        elif val == 'n/a':
            out = 0
        elif val == 'all enabled':
            out = 2 ** size
        else:
            raise NotImplementedError
        return out

    def read_spreadsheet(self, sh):
        content = sh.get_all_values()

        registers = []
        for row in content:
            if row[3] == '' or row[3] == 'Register Name':
                continue
            try:
                name = row[3]
                address = int(row[2])
                size = int(row[5])
                default = self._interpret_number(row[8], size)
                description = row[7]
                mode = 1 if row[4] == 'R/W' else 0

                registers.append(
                    {'name': name, 'address': address, 'size': size, 'default': default, 'description': description,
                     'mode': mode})
            except Exception as e:
                logger.error("Error handling register: " + name)
                raise Exception(e)

        return registers

    def dump_yaml(self, registers, filename='register_lookup.yaml', default_base=2, address_base=16):
        for reg in registers:
            if default_base == 2:
                reg['default'] = bin(reg['default'])
            elif default_base == 16:
                reg['default'] = hex(reg['default'])
            if address_base == 2:
                reg['address'] = bin(reg['address'])
            elif address_base == 16:
                reg['address'] = hex(reg['address'])

        data = {'registers': registers}

        with open(filename, 'w') as outfile:
            yaml.dump(data, outfile, default_flow_style=False)

        logger.info('Register map successfully dumped!')
        logger.debug(registers)



class RD53ARegisterParser():
    '''
    Uses local lookup file in YAML format to get register properties by name or address
    '''

    def __init__(self, lookup_file=None):
        self.proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        if not lookup_file:
            lookup_file = os.path.join(self.proj_dir, 'bdaq53' + os.sep + 'register_lookup.yaml')

        try:
            self.register_map = self._read_yaml(lookup_file)
        except IOError as e:
            logger.error(e)
            raise IOError(e)


    def _is_int(self, string):
        try:
            int(string)
            return True
        except ValueError:
            return False

    def _is_address(self, value):
        if not (type(value) == int or type(value) == str):
            return False

        try:
            int(value)
            return True
        except ValueError:
            pass
        try:
            int(value, 16)
            return True
        except ValueError:
            pass

        return False

    def _parse_address(self, value):
        if type(value) == int:
            return str(hex(value))
        elif type(value) == str:
            try:
                return str(hex(int(value)))
            except ValueError:
                pass
            try:
                return str(hex(int(value, 16)))
            except ValueError:
                pass

        raise ValueError('Could not parse input %s to valid address!' % value)


    def _read_yaml(self, f):
        with open(f, 'r') as infile:
            y = yaml.load(infile)
        return y['registers']


    def _find_register(self, search_string):
        '''
            Finds a single or multiple registers by name or address and returns all information about the register
            as dict (single register) or list of dicts (multiple registers)

            Parameters:
            ----------
            search_string : str
                    A string to compare with register names

            Returns:
            ----------
            either reg : dict
                    A dictionary containing all information about the register corresponding to search_string
            or multireg : list
                    A list of dictionaries containing all information about multiple registers corresponding to search string
        '''

        multireg = []
        multireg_flag = False

        if self._is_address(search_string):
            address = self._parse_address(search_string)
            for reg in self.register_map:
                if address == reg['address']:
                    return dict(reg)
        else:
            for reg in self.register_map:
                if search_string == reg['name']:
                    return dict(reg)
                elif search_string in reg['name'] and self._is_int(reg['name'][-1:]):
                    multireg_flag = True
                    multireg.append(reg)

                # Exceptional cases
                elif 'HITOR' in search_string and 'MASK_SYNC' in reg['name']:
                    logger.debug('Found multiple registers: %s!' % (reg['name']))
                    multireg.append(reg)
                elif search_string == 'AutoReadA0' and reg['name'] == 'AutoRead0':
                    return dict(reg)
                elif search_string == 'AutoReadB0' and reg['name'] == 'AutoRead1':
                    return dict(reg)
                elif search_string == 'AutoReadA1' and reg['name'] == 'AutoRead2':
                    return dict(reg)
                elif search_string == 'AutoReadB1' and reg['name'] == 'AutoRead3':
                    return dict(reg)
                elif search_string == 'AutoReadA2' and reg['name'] == 'AutoRead4':
                    return dict(reg)
                elif search_string == 'AutoReadB2' and reg['name'] == 'AutoRead5':
                    return dict(reg)
                elif search_string == 'AutoReadA3' and reg['name'] == 'AutoRead6':
                    return dict(reg)
                elif search_string == 'AutoReadB3' and reg['name'] == 'AutoRead7':
                    return dict(reg)

            if multireg_flag:
                multireg = sorted(multireg, key=lambda k: k['name'][-1:])
                return multireg

        raise ValueError('Register not found.')


    def get_address(self, register_name):
        '''
            Returns the address of the register corresponding to register_name

            Parameters:
            ----------
            register_name : str
                    The name of the register to return the address of

            Returns:
            ----------
            address : str
                    The address of the register corresponding to register_name in hex
        '''

        reg = self._find_register(register_name)

        if type(reg) == dict:
            add = reg['address']
            logger.debug("Found single register %s with address %s" % (register_name, add))
            return eval(add)
        elif type(reg) == list:
            add = reg[0]['address']
            logger.debug("Found multiple registers %s with starting address %s" % (register_name, add))
            return eval(add)
        else:
            raise ValueError('Could not determine correct register!')


    def get_name(self, address):
        '''
            Returns the name of the register corresponding to address

            Parameters:
            ----------
            address : str
                    The address of the register to return the name of. Hexadecimal format.

            Returns:
            ----------
            name : str
                    The name of the register corresponding to address.
        '''

        address = self._parse_address(address)

        for reg in self.register_map:
            if address == reg['address']:
                logger.debug('Found single register %s with address %s and size %s!' % (reg['name'], reg['address'], reg['size']))
                return reg['name']

        raise ValueError('Could not determine correct register!')


    def get_size(self, register):
        '''
            Returns the size of the register corresponding to register

            Parameters:
            ----------
            register : str
                    The name or address of the register to return the size of

            Returns:
            ----------
            size : int
                    The size of the register corresponding to register, in case of multiple registers, the total size is returned
        '''

        reg = self._find_register(register)
        if type(reg) == dict:
            logger.debug('Found single register %s with address %s and size %s!' % (reg['name'], reg['address'], reg['size']))
            return int(reg['size'])

        elif type(reg) == list:
            total_size = 0
            for r in reg:
                total_size += int(r['size'])
                logger.debug('Found multiple registers %s with total size %s!' % (register, total_size))
            return total_size

        raise ValueError('Could not determine correct register!')


    def get_default(self, register):
        '''
            Returns the default value of the register corresponding to register

            Parameters:
            ----------
            register : str
                    The name or address of the register to return the default value of

            Returns:
            ----------
            default : str
                    Binary string representation of the default value of the register corresponding to register
        '''

        reg = self._find_register(register)
        if type(reg) == dict:
            logger.debug('Found single register %s with address %s and default value %s!' % (reg['name'], reg['address'], reg['default']))
            return reg['default']
        elif type(reg) == list:
            raise NotImplementedError('This method is only implemented for single registers!')

        raise ValueError('Could not determine correct register!')


    def get_mode(self, register):
        '''
            Returns the mode of the register corresponding to register

            Parameters:
            ----------
            register : str
                    The name or address of the register to return the default value of

            Returns:
            ----------
            mode : bool
                    Boolean representation of register mode. True is Read/Write, False is Read only.
        '''

        reg = self._find_register(register)
        if type(reg) == dict:
            logger.debug('Found single register %s with address %s and mode %s!' % (reg['name'], reg['address'], reg['mode']))
            return bool(reg['mode'])
        elif type(reg) == list:
            raise NotImplementedError('This method is only implemented for single registers!')

        raise ValueError('Could not determine correct register!')



if __name__ == '__main__':
    '''
    Dump the spreadsheet to a lookup file
    '''
    re = RD53ARegisterExtraction()

    sh = re.open_worksheet(credfile='creds.json', key='[Your API key]', worksheet=1)
    registers = re.read_spreadsheet(sh)
    re.dump_yaml(registers, filename='register_lookup.yaml', default_base=2, address_base=16)