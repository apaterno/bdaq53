#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

from __future__ import print_function

import numpy as np
import numba
from numba import njit

# Word defines
USERK_FRAME_ID = 0x01000000
HEADER_ID = 0x00010000


@njit
def translate_mapping(core_column, region, pixel_id):
    '''
        Translate mapping between raw data format (core column and region) and absolute column and row.

        ----------
        Parameters:
            core_column : int
                Core column number [0:49]
            region : int
                Region in the core column [0:383]
            pixel_id : int
                Pixel in the region [0:3]

        Returns:
            column : int
                Absolute column number [0:399]
            row : int
                Absolute row number [0:191]
    '''

    if pixel_id > 3:
        raise ValueError('pixel_id cannot be larger than 3!')
    column = core_column * 8 + pixel_id
    if region % 2 == 1:
        column += 4
    row = int(region / 2)

    return column, row


@njit
def build_event(hits, data_out_i, hist_occ, hist_tot, hist_rel_bcid, hist_scurve, hit_buffer, hit_buffer_i, min_bcid, scan_param_id):
    '''
        Fill result data structures
        called at the end of an event.
    '''

    # Copy hits from buffer to result data structure
    for i in range(hit_buffer_i):
        hits[data_out_i] = hit_buffer[i]
        # Set minimum BCID of event hits
        hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] - min_bcid
        if hits[data_out_i]['rel_bcid'] >= 32: # TODO: Workaround for wrong bcid (on first event?)
            hits[data_out_i]['rel_bcid'] = 0
            
        hits[data_out_i]['scan_param_id'] = scan_param_id
        # Fill histograms
        hist_rel_bcid[hits[data_out_i]['rel_bcid']] += 1
        hist_occ[hits[data_out_i]['col'], hits[data_out_i]['row']] += 1
        hist_tot[hits[data_out_i]['tot']] += 1
        hist_scurve[hits[data_out_i]['col']*192+hits[data_out_i]['row'], scan_param_id] += 1

        data_out_i += 1

    return data_out_i


@njit
def add_hits(data_word, hit_buffer, hit_buffer_i, event_number, trg_id, bcid):
    multicol = (data_word >> 26) & 0x3f
    region = (data_word >> 16) & 0x3ff

    for i in range(4):
        col, row = translate_mapping(multicol, region, i)
        tot = (data_word >> i * 4) & 0xf
        
        if col < 400 and row < 192:
            if tot != 255 and tot != 15:
                hit_buffer[hit_buffer_i]['bcid'] = bcid
                hit_buffer[hit_buffer_i]['event_number'] = event_number
                hit_buffer[hit_buffer_i]['trigger_id'] = trg_id
                hit_buffer[hit_buffer_i]['col'] = col
                hit_buffer[hit_buffer_i]['row'] = row
                hit_buffer[hit_buffer_i]['tot'] = tot
                hit_buffer_i += 1
        else:
            # TODO: handle and log corrupted data
            print('Corrupted data detected!')
            pass

    return hit_buffer_i


@njit
def interpret_data(rawdata, hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve, scan_param_id):
    event_number = 0
    data_cnt = 0
    data_header = False
    data_out_i = 0
    this_bcid = 0
    prev_bcid = -1
    
    # Hit buffer to store actual event hits needed to set parameters calculated at the end of an event
    hit_buffer_i = 0  # Hit buffer index
    hit_buffer = np.empty_like(hits) # possible speedup with np.empty_like(hits)
  
    # Per event variables
    trig_id = 0
    min_bcid = 0
  
    for word in rawdata:
        if (word & USERK_FRAME_ID):  # skip USER_K frame
            continue
              
        if (word & HEADER_ID):  # data header
            data_header = True
            data_cnt = 0
  
        # reassemble full data word from two FPGA data words
        if data_cnt == 0:
            data_word = word & 0xffff
        else:
            data_word = data_word << 16 | word & 0xffff
  
        if data_cnt >= 1:  # full data word
            if (data_header == True):  # data word is a header
                data_header = False
                
                this_bcid = data_word & 0x7fff
                trig_id = (data_word >> 20) & 0x1f
                
                # TODO: corruption check if trig_id is always prev_trig_id +1 
                
                if ((prev_bcid+1) & 0x7fff) != this_bcid:    # when bcid changes for more then 1 -> new event
                    data_out_i = build_event(hits, data_out_i,  # Result hit array to be filled
                                        hist_occ, hist_tot, hist_rel_bcid, hist_scurve,  # Result histograms to be filled
                                        hit_buffer, hit_buffer_i,
                                        min_bcid, scan_param_id)
                    
                    event_number += 1
                          
                    # Reset per event variables
                    hit_buffer_i = 0
                    min_bcid = this_bcid
                
                prev_bcid = this_bcid
                    
            else:  # data word is hit data
                hit_buffer_i = add_hits(data_word,
                                        hit_buffer, hit_buffer_i,
                                        event_number,
                                        trig_id,
                                        this_bcid)
            data_cnt = 0
        else:
            data_cnt += 1
  
    # TODO: Add last event data (to be removed when chunking is implemented
    data_out_i = build_event(hits, data_out_i,  # Result hit array to be filled
                            hist_occ, hist_tot, hist_rel_bcid, hist_scurve,  # Result histograms to be filled
                            hit_buffer, hit_buffer_i,
                            min_bcid, scan_param_id)
  
    return data_out_i


def init_outs(n_hits, n_scan_params):
    hist_occ = np.zeros(shape=(400, 192), dtype=np.uint32)
    hist_tot = np.zeros(shape=(16), dtype=np.uint32)
    hist_rel_bcid = np.zeros(shape=(32), dtype=np.uint32)
    hist_scurve = np.zeros(shape=(76800, n_scan_params), dtype=np.uint32)
    hits = np.zeros(shape=n_hits,
                    dtype={'names': ['event_number', 'trigger_id', 'bcid', 'rel_bcid', 'col', 'row', 'tot', 'scan_param_id'],
                           'formats': ['int64', 'uint8', 'uint16', 'uint8', 'uint16', 'uint16', 'uint8', 'uint8']})

    return hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve


@njit(locals={'cluster_shape': numba.int64})
def calc_cluster_shape(cluster_array):
    '''Boolean 8x8 array to number.
    '''
    cluster_shape = 0
    indices_x, indices_y = np.nonzero(cluster_array)
    for index in np.arange(indices_x.size):
        cluster_shape += 2**xy2d_morton(indices_x[index], indices_y[index])
    return cluster_shape


@njit(numba.int64(numba.uint32, numba.uint32))
def xy2d_morton(x, y):
    ''' Tuple to number.

    See: https://stackoverflow.com/questions/30539347/
         2d-morton-code-encode-decode-64bits
    '''
    x = (x | (x << 16)) & 0x0000FFFF0000FFFF
    x = (x | (x << 8)) & 0x00FF00FF00FF00FF
    x = (x | (x << 4)) & 0x0F0F0F0F0F0F0F0F
    x = (x | (x << 2)) & 0x3333333333333333
    x = (x | (x << 1)) & 0x5555555555555555

    y = (y | (y << 16)) & 0x0000FFFF0000FFFF
    y = (y | (y << 8)) & 0x00FF00FF00FF00FF
    y = (y | (y << 4)) & 0x0F0F0F0F0F0F0F0F
    y = (y | (y << 2)) & 0x3333333333333333
    y = (y | (y << 1)) & 0x5555555555555555

    return x | (y << 1)
