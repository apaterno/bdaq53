#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Script to convert the raw data and to plot all histograms
'''

from __future__ import division

import zlib # workaround for matplotlib segmentation fault
import shutil
import os.path
import numpy as np
import logging
import yaml
import multiprocessing as mp
from functools import partial
import numba
import tables as tb
from scipy.optimize import curve_fit
from scipy.special import erf
from matplotlib.backends.backend_pdf import PdfPages
from pixel_clusterizer.clusterizer import HitClusterizer

from tqdm import tqdm

from bdaq53.analysis import analysis_utils
from bdaq53.analysis import plotting


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

np.warnings.filterwarnings('ignore')

def scurve(x, A, mu, sigma):
    return 0.5 * A * erf((x - mu) / (np.sqrt(2) * sigma)) + 0.5 * A

def zcurve(x, A, mu, sigma):
    return -0.5 * A * erf((x - mu) / (np.sqrt(2) * sigma)) + 0.5 * A


def fit_scurve(scurve_data, scan_param_range, invert_x):
    '''
        Data of some pixels to fit, has to be global for the multiprocessing module
    '''
    index = np.argmax(np.abs(np.diff(scurve_data))) if invert_x else np.argmax(np.diff(scurve_data))
    max_occ = np.max(scurve_data) if invert_x else np.median(scurve_data[index:])
    threshold = scan_param_range[index]
    # or index == 0: occupancy is zero or close to zero
    if abs(max_occ) <= 1e-08:
        popt = [0, 0, 0]
    else:
        try:
            if invert_x:
                popt, _ = curve_fit(zcurve, scan_param_range, scurve_data, p0=[max_occ, threshold, 2.5], check_finite=False)
            else:
                popt, _ = curve_fit(scurve, scan_param_range, scurve_data, p0=[max_occ, threshold, 2.5], check_finite=False)
        except RuntimeError:  # fit failed
            popt = [0, 0, 0]
    if popt[1] < 0:  # threshold < 0 rarely happens if fit does not work
        popt = [0, 0, 0]
    return popt[1:3]


class AnalyzeRawData(object):
    """
        A class to analyze RD53A raw data
    """

    def __init__(self, raw_data_file=None, analyzed_data_file=None,
                 create_pdf=True, scan_parameter_name=None,
                 cluster_hits=False, chunk_size=100000000):
        ''' 
            Parameters
            ----------
            raw_data_file : string or tuple, list
                A string or a list of strings with the raw data file name(s). File ending (.h5)
                does not not have to be set.
            analyzed_data_file : string
                The file name of the output analyzed data file. File ending (.h5)
                Does not have to be set.
            create_pdf : boolean
                Creates interpretation plots into one PDF file. Only active if raw_data_file is given.
            scan_parameter_name : string or iterable
                The name/names of scan parameter(s) to be used during analysis. If not set the scan parameter
                table is used to extract the scan parameters. Otherwise no scan parameter is set.
            cluster_hits : boolean
                Create cluster table, histograms and plots
        '''
        # Parameters
        self.raw_data_file = raw_data_file
        self.analyzed_data_file = analyzed_data_file
        self.create_pdf = create_pdf
        self.scan_parameter_name = scan_parameter_name
        self.cluster_hits = cluster_hits
        self.chunk_size = chunk_size
        self.output_pdf = None
        
        if not os.path.isfile(raw_data_file):
            raise IOError('Raw data file does not exist.')

        if not self.analyzed_data_file:
            self.analyzed_data_file = raw_data_file[:-3] + '_interpreted.h5'
            
        if self.create_pdf:
            self.output_pdf_path = self.analyzed_data_file[:-3] + '.pdf'
            self.output_pdf = PdfPages(self.output_pdf_path)

        # Hit buffer for word at event alignment
        self.last_words = None
        
        self._setup_clusterizer()


    def __enter__(self):
        return self


    def __exit__(self, *exc_info):
        if self.output_pdf is not None and isinstance(self.output_pdf, PdfPages):
            logger.info( 'Closing output PDF file: %s', str(self.output_pdf._file.fh.name))
            self.output_pdf.close()
            shutil.copyfile(self.output_pdf_path, os.path.join(os.path.split(self.output_pdf_path)[0], 'last_scan.pdf'))

    def _setup_clusterizer(self):
        ''' Define data structure and settings for hit clusterizer package '''
        # Define all field names and data types
        hit_fields = {'event_number': 'event_number',
                      'trigger_id': 'trigger_id',
                      'bcid': 'bcid',
                      'rel_bcid': 'frame',
                      'col': 'column',
                      'row': 'row',
                      'tot': 'charge',
                      'scan_param_id': 'scan_param_id'
                      }
        hit_dtype = np.dtype([('event_number', '<i8'),
                              ('trigger_id', 'u1'),
                              ('bcid', '<u2'),
                              ('rel_bcid', 'u1'),
                              ('col', '<u2'),
                              ('row', '<u2'),
                              ('tot', 'u1'),
                              ('scan_param_id', 'u1')])
        cluster_fields = {'event_number': 'event_number',
                          'column': 'column',
                          'row': 'row',
                          'size': 'n_hits',
                          'id': 'ID',
                          'tot': 'charge',
                          'scan_param_id': 'scan_param_id',
                          'seed_col': 'seed_column',
                          'seed_row': 'seed_row',
                          'mean_col': 'mean_column',
                          'mean_row': 'mean_row'}
        self.cluster_dtype = np.dtype([('event_number', '<i8'),
                                       ('id', '<u2'),
                                       ('size', '<u2'),
                                       ('tot', '<u2'),
                                       ('seed_col', '<u1'),
                                       ('seed_row', '<u2'),
                                       ('mean_col', '<f4'),
                                       ('mean_row', '<f4'),
                                       ('dist_col', '<u4'),
                                       ('dist_row', '<u4'),
                                       ('cluster_shape', '<i8'),
                                       ('scan_param_id', 'u1')])

        if self.cluster_hits:  # Allow analysis without clusterizer installed
            # Define end of cluster function to calculate cluster shape
            # and cluster distance in column and row direction
            def end_of_cluster_function(hits, clusters, cluster_size,
                                        cluster_hit_indices, cluster_index,
                                        cluster_id, charge_correction,
                                        noisy_pixels, disabled_pixels,
                                        seed_hit_index):
                hit_arr = np.zeros((15, 15), dtype=np.bool_)
                center_col = hits[cluster_hit_indices[0]].column
                center_row = hits[cluster_hit_indices[0]].row
                hit_arr[7, 7] = 1
                min_col = hits[cluster_hit_indices[0]].column
                max_col = hits[cluster_hit_indices[0]].column
                min_row = hits[cluster_hit_indices[0]].row
                max_row = hits[cluster_hit_indices[0]].row
                for i in cluster_hit_indices[1:]:
                    if i < 0:  # Not used indeces = -1
                        break
                    diff_col = np.int32(hits[i].column - center_col)
                    diff_row = np.int32(hits[i].row - center_row)
                    if np.abs(diff_col) < 8 and np.abs(diff_row) < 8:
                        hit_arr[7 + hits[i].column - center_col,
                                7 + hits[i].row - center_row] = 1
                    if hits[i].column < min_col:
                        min_col = hits[i].column
                    if hits[i].column > max_col:
                        max_col = hits[i].column
                    if hits[i].row < min_row:
                        min_row = hits[i].row
                    if hits[i].row > max_row:
                        max_row = hits[i].row

                if max_col - min_col < 8 and max_row - min_row < 8:
                    # Make 8x8 array
                    col_base = 7 + min_col - center_col
                    row_base = 7 + min_row - center_row
                    cluster_arr = hit_arr[col_base:col_base + 8,
                                          row_base:row_base + 8]
                    # Finally calculate cluster shape
                    # uint64 desired, but numexpr and others limited to int64
                    if cluster_arr[7, 7] == 1:
                        cluster_shape = np.int64(-1)
                    else:
                        cluster_shape = np.int64(analysis_utils.calc_cluster_shape(cluster_arr))
                else:
                    # Cluster is exceeding 8x8 array
                    cluster_shape = np.int64(-1)

                clusters[cluster_index].cluster_shape = cluster_shape
                clusters[cluster_index].dist_col = max_col - min_col + 1
                clusters[cluster_index].dist_row = max_row - min_row + 1

            # Initialize clusterizer with custom hit/cluster fields
            self.clz = HitClusterizer(
                hit_fields=hit_fields,
                hit_dtype=hit_dtype,
                cluster_fields=cluster_fields,
                cluster_dtype=self.cluster_dtype,
                min_hit_charge=0,
                max_hit_charge=13,
                column_cluster_distance=3,
                row_cluster_distance=3,
                frame_cluster_distance=2,
                ignore_same_hits=True)

            # Set end_of_cluster function for shape and distance calculation
            self.clz.set_end_of_cluster_function(end_of_cluster_function)

    def _range_of_parameter(self, meta_data):
        _, index = np.unique(meta_data['scan_param_id'], return_index=True)
        start = meta_data[index]['index_start']
        stop = np.append(start[:-1] + np.diff(start), meta_data[-1]['index_stop'])
        return np.column_stack((start, stop))


    def _words_per_parameter(self, par_range, data, chunk_size):
        for i in range(0, data.shape[0], chunk_size):
            words = data[i:i+chunk_size]
            for start, stop in par_range:
                yield words[start:stop]

            
    def analyze_data(self):
        '''
        '''
        with tb.open_file(self.raw_data_file) as in_file:
            n_words = in_file.root.raw_data.shape[0]
            meta_data = in_file.root.meta_data[:]
            par_range = self._range_of_parameter(meta_data)
            hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve = analysis_utils.init_outs(n_hits=self.chunk_size, n_scan_params=max(np.unique(meta_data['scan_param_id']))+1)
            
            with tb.open_file(self.analyzed_data_file, 'w') as out_file:
                hit_table = out_file.create_table(out_file.root, name='Hits',
                                                  description=hits.dtype,
                                                  title='hit_data',
                                                  filters=tb.Filters(complib='blosc',
                                                                     complevel=5,
                                                                     fletcher32=False))
                
                hit_table.attrs.scan_id = in_file.root.meta_data.attrs.scan_id
                hit_table.attrs.run_name = in_file.root.meta_data.attrs.run_name
                hit_table.attrs.kwargs = in_file.root.meta_data.attrs.kwargs
                hit_table.attrs.dacs = in_file.root.meta_data.attrs.dacs
                hit_table.attrs.software_version = in_file.root.meta_data.attrs.software_version
 
                if self.cluster_hits:
                    cluster_table = out_file.create_table(
                                        out_file.root, name='Cluster',
                                        description=self.cluster_dtype,
                                        title='Cluster',
                                        filters=tb.Filters(complib='blosc',
                                                           complevel=5,
                                                           fletcher32=False))
                    hist_cs_size = np.zeros(shape=(100, ), dtype=np.uint16)
                    hist_cs_tot = np.zeros(shape=(100, ), dtype=np.uint16)
                    hist_cs_shape = np.zeros(shape=(300, ), dtype=np.int16)
 
                pbar = tqdm(total=n_words)
                for scan_param_id, words in enumerate(self._words_per_parameter(par_range, in_file.root.raw_data, self.chunk_size)):
                    n_hits = analysis_utils.interpret_data(rawdata=words,
                                                           hits=hits,
                                                           hist_occ=hist_occ,
                                                           hist_tot=hist_tot,
                                                           hist_rel_bcid=hist_rel_bcid,
                                                           hist_scurve=hist_scurve,
                                                           scan_param_id=scan_param_id)
                    hit_table.append(hits[:n_hits])
                    hit_table.flush()

                    if self.cluster_hits:
                        _, cluster = self.clz.cluster_hits(hits[:n_hits])
                        cluster_table.append(cluster)
                        hist_cs_size += np.bincount(cluster['size'], minlength=100)[:100].astype(np.uint16)
                        hist_cs_tot += np.bincount(cluster['tot'], minlength=100)[:100].astype(np.uint16)
                        hist_cs_shape += np.bincount(cluster['cluster_shape'], minlength=300)[:300].astype(np.uint16)

                    pbar.update(words.shape[0])
                pbar.close()
        
        self._create_additional_hit_data(hist_occ, hist_tot, hist_rel_bcid, hist_scurve)
        if self.cluster_hits:
            self._create_additional_cluster_data(hist_cs_size, hist_cs_tot, hist_cs_shape)
            
        return hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve
            

    def _create_additional_hit_data(self, hist_occ, hist_tot, hist_rel_bcid, hist_scurve):
        with tb.open_file(self.analyzed_data_file, 'r+') as out_file:
            attributes = yaml.load(out_file.root.Hits.attrs.kwargs)
            scan_id = out_file.root.Hits.attrs.scan_id
            
            out_file.create_carray(out_file.root,
                                   name='HistOcc',
                                   title='Occupancy Histogram',
                                   obj=hist_occ,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistTot',
                                   title='ToT Histogram',
                                   obj=hist_tot,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistRelBCID',
                                   title='Relativ BCID Histogram',
                                   obj=hist_rel_bcid,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            
            if scan_id == 'threshold_scan' or scan_id == 'global_threshold_tuning' or scan_id == 'local_threshold_tuning':
                out_file.create_carray(out_file.root,
                                       name='HistSCurve',
                                       title='Scruve Data',
                                       obj=hist_scurve,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))
                
                if scan_id == 'threshold_scan': 
                    scan_param_range = [v-attributes.get('VCAL_MED') for v in range(attributes.get('VCAL_HIGH_start'), attributes.get('VCAL_HIGH_stop'), attributes.get('VCAL_HIGH_step'))]
                    self.threshold_map, self.noise_map = self.fit_scurves_multithread(hist_scurve, scan_param_range)
                elif scan_id == 'global_threshold_tuning':
                    scan_param_range = range(attributes.get('Vthreshold_LIN_start'),
                                        attributes.get('Vthreshold_LIN_stop'),
                                        attributes.get('Vthreshold_LIN_step'))
                    self.threshold_map, self.noise_map = self.fit_scurves_multithread(hist_scurve, scan_param_range, invert_x=True)
                elif scan_id == 'local_threshold_tuning':
                    scan_param_range = range(16)
                    self.threshold_map, self.noise_map = self.fit_scurves_multithread(hist_scurve, scan_param_range, invert_x=False)
                
                out_file.create_carray(out_file.root,
                                       name='ThresholdMap',
                                       title='Threshold Map',
                                       obj=self.threshold_map,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))
                out_file.create_carray(out_file.root,
                                       name='NoiseMap',
                                       title='Noise Map',
                                       obj=self.noise_map,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))


    def _create_additional_cluster_data(self, hist_cs_size, hist_cs_tot, hist_cs_shape):
        '''
            Store cluster histograms in analyzed data file
        '''
        with tb.open_file(self.analyzed_data_file, 'r+') as out_file:
            out_file.create_carray(out_file.root,
                                   name='HistClusterSize',
                                   title='Cluster Size Histogram',
                                   obj=hist_cs_size,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistClusterTot',
                                   title='Cluster ToT Histogram',
                                   obj=hist_cs_tot,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistClusterShape',
                                   title='Cluster Shape Histogram',
                                   obj=hist_cs_shape,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))


    def fit_scurves_multithread(self, scurves, scan_param_range=None, invert_x=False):
        logger.info("Start S-curve fit on %d CPU core(s)", mp.cpu_count())
    
        # remove 0 ?
        #sum = np.sum(scurves,axis=1)
        #scurves = scurves[sum>0]
    
        # trick to give a function more than one parameter, needed for pool.map
        partialfit_scurve = partial(fit_scurve, scan_param_range=scan_param_range, invert_x=invert_x)
        # create as many workers as physical cores are available
        pool = mp.Pool()
        try:
            result_list = pool.map(partialfit_scurve, scurves.tolist())
        finally:
            pool.close()
            pool.join()
        result_array = np.array(result_list)
        logger.info("S-curve fit finished")
        
        thr = result_array[:,0]
        sig = result_array[:,1]
        thr2D = np.reshape(thr, (50*8, 192))
        sig2D = np.reshape(sig, (50*8, 192))
        return thr2D, sig2D
    
    
    ''' User callable plotting functions '''
    
    def create_parameter_page(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            attributes = yaml.load(in_file.root.Hits.attrs.kwargs)
            dacs = yaml.load(in_file.root.Hits.attrs.dacs)
            run_name = in_file.root.Hits.attrs.run_name
            scan_id = in_file.root.Hits.attrs.scan_id
            software_version = in_file.root.Hits.attrs.software_version
            
        plotting.write_parameters(scan_id, run_name, attributes, dacs, software_version, filename=self.output_pdf)

    def create_occupancy_map(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            plotting.plot_occupancy(hist=in_file.root.HistOcc[:].T, filename=self.output_pdf)
            
    def create_tot_plot(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            plotting.plot_tot(hist=in_file.root.HistTot[:].T, filename=self.output_pdf)
            
    def create_rel_bcid_plot(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            plotting.plot_relative_bcid(hist=in_file.root.HistRelBCID[:].T, filename=self.output_pdf)
            
    def create_scurves_plot(self, scan_parameter_name=None):
        if scan_parameter_name is None:
            scan_parameter_name = 'Scan parameter'
        with tb.open_file(self.analyzed_data_file) as in_file:
            scan_id = in_file.root.Hits.attrs.scan_id
            attributes = yaml.load(in_file.root.Hits.attrs.kwargs)
            if scan_id == 'threshold_scan':
                scan_parameter_range = [v-attributes.get('VCAL_MED') for v in range(attributes.get('VCAL_HIGH_start'),
                                        attributes.get('VCAL_HIGH_stop'),
                                        attributes.get('VCAL_HIGH_step'))]
                scan_parameter_name = '$\Delta$ VCAL'
            elif scan_id == 'global_threshold_tuning':
                scan_parameter_range = range(attributes.get('Vthreshold_LIN_start'),
                                        attributes.get('Vthreshold_LIN_stop'),
                                        attributes.get('Vthreshold_LIN_step'))
            elif scan_id == 'local_threshold_tuning':
                scan_parameter_range = range(16)
                
            plotting.plot_scurves(scurves=in_file.root.HistSCurve[:].T,
                                  scan_parameters=scan_parameter_range,
                                  start_column=attributes['start_column'],
                                  stop_column=attributes['stop_column'],
                                  scan_parameter_name=scan_parameter_name,
                                  filename=self.output_pdf)
                
    def create_threshold_plot(self, scan_parameter_name=None):
        if scan_parameter_name is None:
            scan_parameter_name = 'Scan parameter'
        with tb.open_file(self.analyzed_data_file) as in_file:
            scan_id = in_file.root.Hits.attrs.scan_id
            attributes = yaml.load(in_file.root.Hits.attrs.kwargs)
            if scan_id == 'threshold_scan':
                plot_range = [v-attributes.get('VCAL_MED') for v in range(attributes.get('VCAL_HIGH_start'),
                                        attributes.get('VCAL_HIGH_stop'),
                                        attributes.get('VCAL_HIGH_step'))]
                scan_parameter_name = '$\Delta$ VCAL'
            elif scan_id == 'global_threshold_tuning':
                plot_range = range(attributes.get('Vthreshold_LIN_start'),
                                        attributes.get('Vthreshold_LIN_stop'),
                                        attributes.get('Vthreshold_LIN_step'))
            plotting.plot_distribution(in_file.root.ThresholdMap[:].T,
                                   start_column=attributes['start_column'],
                                   stop_column=attributes['stop_column'],
                                   plot_range=plot_range,
                                   title='Threshold distribution',
                                   x_axis_title=scan_parameter_name,
                                   y_axis_title='#',
                                   filename=self.output_pdf)
            
    def create_noise_plot(self, scan_parameter_name=None):
        if scan_parameter_name is None:
            scan_parameter_name = 'Scan parameter'
        with tb.open_file(self.analyzed_data_file) as in_file:
            attributes = yaml.load(in_file.root.Hits.attrs.kwargs)
            plotting.plot_distribution(in_file.root.NoiseMap[:].T,
                                       start_column=attributes['start_column'],
                                       stop_column=attributes['stop_column'],
                                       title='Noise distribution',
                                       x_axis_title=scan_parameter_name,
                                       y_axis_title='#',
                                       filename=self.output_pdf)
            
    def create_threshold_map(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            plotting.plot_occupancy(hist=in_file.root.ThresholdMap[:].T, z_label='Threshold', title='Threshold', filename=self.output_pdf)
            
    def create_noise_map(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            plotting.plot_occupancy(hist=in_file.root.NoiseMap[:].T, z_label='Noise', z_max='median', title='Noise', filename=self.output_pdf)
            
    def create_cluster_size_plot(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            plotting.plot_cl_size(in_file.root.HistClusterSize[:], filename=self.output_pdf)
            
    def create_cluster_tot_plot(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            plotting.plot_cl_tot(in_file.root.HistClusterTot[:], filename=self.output_pdf)
            
    def create_cluster_shape_plot(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            plotting.plot_cl_shape(in_file.root.HistClusterShape[:], filename=self.output_pdf)
            

    

if __name__ == "__main__":
    pass
