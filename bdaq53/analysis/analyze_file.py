#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Run customized analysis on a specified raw data file
'''

import logging
from bdaq53.analysis import analysis

logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(name)s - [%(levelname)-8s] (%(threadName)-10s) %(message)s")

raw_data_file = ''


with analysis.AnalyzeRawData(raw_data_file=raw_data_file, analyzed_data_file=None,
                             create_pdf=True, scan_parameter_name=None,
                             cluster_hits=False, chunk_size=100000000) as a:
    
    logging.info('Analyzing data...')
    a.analyze_data()
    
    logging.info('Creating selected plots...')
    a.create_parameter_page()
    a.create_occupancy_map()
    a.create_tot_plot()
    a.create_rel_bcid_plot()
    a.create_scurves_plot()
    a.create_threshold_plot()
    a.create_threshold_map()
    a.create_noise_plot(scan_parameter_name='$\Delta VCAL')
    a.create_noise_map()
    if a.cluster_hits:
        a.create_cluster_size_plot()
        a.create_cluster_tot_plot()
        a.create_cluster_shape_plot()
        