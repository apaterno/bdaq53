#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import numpy as np
import math
import logging
from collections import OrderedDict

from scipy.optimize import curve_fit
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import colors, cm
from matplotlib.backends.backend_pdf import PdfPages


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def gauss(x, *p):
    amplitude, mu, sigma = p
    return amplitude * np.exp(- (x - mu)**2.0 / (2.0 * sigma**2.0))
#     mu, sigma = p
#     return 1.0 / (sigma * np.sqrt(2.0 * np.pi)) * np.exp(- (x - mu)**2.0 / (2.0 * sigma**2.0))


def write_parameters(scan_id, run_name, attributes, dacs, sw_ver, filename=None):
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)
    ax.axis('off')

    ax.text(0.01, 1, 'This is a bdaq53 %s with run name\n%s\nusing parameters:' % (scan_id, run_name), fontsize=10)
    ax.text(0.9, -0.11, 'Software version: %s' % (sw_ver), fontsize=3)
    
    tb_dict = OrderedDict(sorted(dacs.items()))
    attr = OrderedDict(sorted(attributes.items()))
    for key, value in attr.items():
        tb_dict[key] = str(value) + '*'
    
    tb_list = []
    for i in range(0,len(tb_dict.keys()),2):
        try:
            key1 = tb_dict.keys()[i]
            key2 = tb_dict.keys()[i+1]
            value1 = tb_dict[key1]
            value2 = tb_dict[key2]
            tb_list.append([key1, value1, '', key2, value2])
        except:
            pass
        
    widths = [0.3,0.1, 0.1, 0.3,0.1]
    labels = ['Parameter', 'Value', '', 'Parameter', 'Value']
    table = ax.table(cellText=tb_list, colWidths=widths, colLabels=labels, cellLoc='left', loc='center')
    table.scale(0.8, 0.8)
    
    for key, cell in table.get_celld().items():
        row, col = key
        if row == 0:
            cell.set_color('grey')
        if col == 2:
            cell.set_color('white')
 
    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)


def plot_1d_hist(hist, yerr=None, title=None, x_axis_title=None, y_axis_title=None, x_ticks=None, color='r', plot_range=None, log_y=False, filename=None):
#     logging.info('Plot 1d histogram%s', (': ' + title.replace('\n', ' ')) if title is not None else '')
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)
    hist = np.array(hist)
    if plot_range is None:
        plot_range = range(0, len(hist))
    plot_range = np.array(plot_range)
    plot_range = plot_range[plot_range < len(hist)]
    if yerr is not None:
        ax.bar(x=plot_range, height=hist[plot_range], color=color, align='center', yerr=yerr)
    else:
        ax.bar(x=plot_range, height=hist[plot_range], color=color, align='center')
    ax.set_xlim((min(plot_range) - 0.5, max(plot_range) + 0.5))
    ax.set_title(title)
    if x_axis_title is not None:
        ax.set_xlabel(x_axis_title)
    if y_axis_title is not None:
        ax.set_ylabel(y_axis_title)
    if x_ticks is not None:
        ax.set_xticks(plot_range)
        ax.set_xticklabels(x_ticks)
        ax.tick_params(which='both', labelsize=8)
    if np.allclose(hist, 0.0):
        ax.set_ylim((0, 1))
    else:
        if log_y:
            ax.set_yscale('log')
    ax.grid(True)
    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)
        

def plot_tot(hist, title=None, filename=None):
    plot_1d_hist(hist=hist, title=('Time-over-Threshold distribution' + r' ($\Sigma$ = %d)' % (np.sum(hist))) if title is None else title, plot_range=range(0, 16), x_axis_title='ToT code [25 ns]', y_axis_title='#', color='b', filename=filename)


def plot_relative_bcid(hist, title=None, filename=None):
    plot_1d_hist(hist=hist, title=('Relative BCID' + r' ($\Sigma$ = %d)' % (np.sum(hist))) if title is None else title, log_y=True, plot_range=None, x_axis_title='Relative BCID [25 ns]', y_axis_title='#', filename=filename)


def plot_cl_size(hist, filename):
    ''' Create 1D cluster size plot w/wo log y-scale '''
    plot_1d_hist(hist=hist, title='Cluster size',
                 log_y=False, plot_range=range(0, 10),
                 x_axis_title='Cluster size',
                 y_axis_title='#', filename=filename)
    plot_1d_hist(hist=hist, title='Cluster size (log)',
                 log_y=True, plot_range=range(0, 100),
                 x_axis_title='Cluster size',
                 y_axis_title='#', filename=filename)


def plot_cl_tot(hist, filename):
    ''' Create 1D cluster size plot w/wo log y-scale '''
    plot_1d_hist(hist=hist, title='Cluster ToT',
                 log_y=False, plot_range=range(0, 96),
                 x_axis_title='Cluster ToT [25 ns]',
                 y_axis_title='#', filename=filename)


def plot_cl_shape(hist, filename):
    ''' Create a histogram with selected cluster shapes '''
    x = np.arange(12)
    fig = Figure()
    _ = FigureCanvas(fig)
    ax = fig.add_subplot(111)
    selected_clusters = hist[[1, 3, 5, 6, 9, 13, 14, 7, 11, 19, 261, 15]]
    ax.bar(x, selected_clusters, align='center')
    ax.xaxis.set_ticks(x)
    fig.subplots_adjust(bottom=0.2)
    ax.set_xticklabels([u"\u2004\u2596",
                        # 2 hit cluster, horizontal
                        u"\u2597\u2009\u2596",
                        # 2 hit cluster, vertical
                        u"\u2004\u2596\n\u2004\u2598",
                        u"\u259e",  # 2 hit cluster
                        u"\u259a",  # 2 hit cluster
                        u"\u2599",  # 3 hit cluster, L
                        u"\u259f",  # 3 hit cluster
                        u"\u259b",  # 3 hit cluster
                        u"\u259c",  # 3 hit cluster
                        # 3 hit cluster, horizontal
                        u"\u2004\u2596\u2596\u2596",
                        # 3 hit cluster, vertical
                        u"\u2004\u2596\n\u2004\u2596\n\u2004\u2596",
                        # 4 hit cluster
                        u"\u2597\u2009\u2596\n\u259d\u2009\u2598"])
    ax.set_title('Cluster shapes')
    ax.set_xlabel('Cluster shape')
    ax.set_ylabel('#')
    ax.grid(True)
    ax.set_yscale('log')
    ax.set_ylim(ymin=1e-1)

    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)


def plot_occupancy(hist, title='Occupancy', z_label='#', z_max=None, filename=None):
    if z_max == 'median':
        z_max = 2 * np.ma.median(hist)
    elif z_max == 'maximum' or z_max is None:
        z_max = np.ma.max(hist)
    if z_max < 1 or hist.all() is np.ma.masked:
        z_max = 1.0

    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)
    ax.set_adjustable('box-forced')
    extent = [0.5, 400.5, 192.5, 0.5]
    bounds = np.linspace(start=0, stop=z_max, num=255, endpoint=True)
    if z_max == 'median':
        cmap = cm.get_cmap('plasma')
    else:
        cmap = cm.get_cmap('plasma')
    cmap.set_bad('w', 1.0)
    norm = colors.BoundaryNorm(bounds, cmap.N)

    im = ax.imshow(hist, interpolation='none', aspect='auto', cmap=cmap, norm=norm, extent=extent)  # TODO: use pcolor or pcolormesh
    ax.set_ylim((192.5, 0.5))
    ax.set_xlim((0.5, 400.5))
    ax.set_title(title + r' ($\Sigma$ = {0})'.format((0 if hist.all() is np.ma.masked else np.ma.sum(hist))))
    ax.set_xlabel('Column')
    ax.set_ylabel('Row')

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cb = fig.colorbar(im, cax=cax, ticks=np.linspace(start=0, stop=z_max, num=9, endpoint=True))
    cb.set_label(z_label)

    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)
        
        
def plot_scurves(scurves, scan_parameters, start_column, stop_column, title='S-curves', ylabel='Occupancy', scan_parameter_name=None, min_x=None, max_x=None, extend_bin_width=True, filename=None):
    max_occ = np.max(scurves) + 5 #Maybe?
    x_bins = scan_parameters#np.arange(-0.5, max(scan_parameters) + 1.5)
    y_bins = np.arange(-0.5, max_occ + 0.5)
    n_pixel = (stop_column - start_column) * 192
    
    param_count = scurves.shape[0]
    hist = np.empty([param_count, max_occ], dtype=np.uint32)
    
    for param in range(param_count):
        hist[param] = np.bincount(scurves[param,start_column*192:(stop_column-1)*192+192], minlength=max_occ)

    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)
    fig.patch.set_facecolor('white')
    cmap = cm.get_cmap('cool')
    if np.allclose(hist, 0.0) or hist.max() <= 1:
        z_max = 1.0
    else:
        z_max = hist.max()
    # for small z use linear scale, otherwise log scale
    if z_max <= 10.0:
        bounds = np.linspace(start=0.0, stop=z_max, num=255, endpoint=True)
        norm = colors.BoundaryNorm(bounds, cmap.N)
    else:
        bounds = np.linspace(start=1.0, stop=z_max, num=255, endpoint=True)
        norm = colors.LogNorm()

    im = ax.pcolormesh(x_bins, y_bins, hist.T, norm=norm)
    
    if z_max <= 10.0:
        cb = fig.colorbar(im, ticks=np.linspace(start=0.0, stop=z_max, num=min(11, math.ceil(z_max) + 1), endpoint=True), fraction=0.04, pad=0.05)
    else:
        cb = fig.colorbar(im, fraction=0.04, pad=0.05)
    cb.set_label("#")
    ax.set_title(title + ' for %d pixel(s)' % (n_pixel))
    if scan_parameter_name is None:
        ax.set_xlabel('Scan parameter')
    else:
        ax.set_xlabel(scan_parameter_name)
    ax.set_ylabel(ylabel)
    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)

def plot_distribution(data, start_column, stop_column, plot_range=None, x_axis_title=None, y_axis_title=None, x_ticks=None, title=None, filename=None):
    data = data[:,start_column:stop_column]
    
    if plot_range is None:  
        if int(np.amax(data)) > np.median(data)*5:
            plot_range = range(int(np.min(data)), int(np.median(data)*5))
        else:
            plot_range = range(int(np.min(data)), int(np.max(data)))
        
    tick_size = plot_range[1] - plot_range[0]
    
    hist, bins =  np.histogram(np.ravel(data), bins=plot_range)
    
    bin_centres = (bins[:-1] + bins[1:]) / 2
    amplitude = np.amax(hist)
    p0 = (amplitude, np.median(hist), (max(plot_range)-min(plot_range))/3)
    
    try:
        coeff, _ = curve_fit(gauss, bin_centres, hist, p0=p0)
    except:
        coeff = None
        logger.error('Gauss fit failed!')
        
    if coeff is not None:
        points = np.linspace(min(plot_range), max(plot_range), 500)
        gau = gauss(points, *coeff)
    
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)
    
    ax.bar(bins[:-1], hist, width=tick_size, align='center')
    if coeff is not None:
        ax.plot(points, gau, "r-", label='Normal distribution')

    ax.set_xlim((min(plot_range) - 0.5, max(plot_range) + 0.5))
    ax.set_title(title)
    if x_axis_title is not None:
        ax.set_xlabel(x_axis_title)
    if y_axis_title is not None:
        ax.set_ylabel(y_axis_title)
    ax.grid(True)
    
    if coeff is not None:
        textright = '$\mu=%.2f$\n$\sigma=%.2f$' % (coeff[1], coeff[2])
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax.text(0.85, 0.9, textright, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)
    
    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)

if __name__ == "__main__":
    pass
