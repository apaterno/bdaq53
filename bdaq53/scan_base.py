#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import zlib # workaround for matplotlib segmentation fault
import time
import os
import tables as tb
import yaml
import logging
import subprocess
import basil
import zmq
from contextlib import contextmanager

from rd53a import rd53a
from fifo_readout import FifoReadout

import pkg_resources
VERSION = pkg_resources.get_distribution("bdaq53").version


def get_software_version():
    try:
        rev = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).strip()
        branch = subprocess.check_output(['git', 'rev-parse', '--abbrev-ref', 'HEAD']).strip()
        return branch + '@' + rev
    except:
        return VERSION


class MetaTable(tb.IsDescription):
    index_start = tb.UInt32Col(pos=0)
    index_stop = tb.UInt32Col(pos=1)
    data_length = tb.UInt32Col(pos=2)
    timestamp_start = tb.Float64Col(pos=3)
    timestamp_stop = tb.Float64Col(pos=4)
    scan_param_id = tb.UInt16Col(pos=5)
    error = tb.UInt32Col(pos=6)
    trigger = tb.Float64Col(pos=7)


def send_data(socket, data, scan_par_id, name='ReadoutData'):
    '''Sends the data of every read out (raw data and meta data)

        via ZeroMQ to a specified socket
    '''

    data_meta_data = dict(
        name=name,
        dtype=str(data[0].dtype),
        shape=data[0].shape,
        timestamp_start=data[1],  # float
        timestamp_stop=data[2],  # float
        error=data[3],  # int
        scan_par_id=scan_par_id
    )
    try:
        socket.send_json(data_meta_data,flags=zmq.SNDMORE | zmq.NOBLOCK)
        # PyZMQ supports sending numpy arrays without copying any data
        socket.send(data[0], flags=zmq.NOBLOCK)
    except zmq.Again:
        pass


class ScanBase(object):
    '''
        Basic run meta class.
        Base class for scan- / tune- / analyze-class.
    '''

    def __init__(self, dut_conf=None):
        logging.info('Initializing %s', self.__class__.__name__)
        self.chip = rd53a(dut_conf)
        
        self.working_dir = os.path.join(os.getcwd(), "output_data")
        if not os.path.exists(self.working_dir):
            os.makedirs(self.working_dir)
            
        self.run_name = time.strftime("%Y%m%d_%H%M%S_") + self.scan_id
        self.output_filename = os.path.join(self.working_dir, self.run_name)
        
        self.fh = logging.FileHandler(self.output_filename + '.log')
        self.fh.setLevel(logging.DEBUG)
        self.logger = logging.getLogger()
        self.logger.addHandler(self.fh)



    def get_basil_dir(self):
        return str(os.path.dirname(os.path.dirname(basil.__file__)))

    
    def get_chip(self):
        return self.chip


    def prepare_masks(self, col_list, mask_steps):
        self.logger.info('Preparing masks...')
        mask_data = []
        for col in col_list:
            for m in range(mask_steps):
                self.chip.reset_masks(tdac=False)
                self.chip.enable_mask[col[0]:col[0]+col[1],:] = True
                
                # some magic to enable injection not in the same island 
                self.chip.injection_mask[col[0]:col[0]+col[1]:2, m::mask_steps] = True
                self.chip.injection_mask[col[0]+1:col[0]+col[1]:2, (m+2)%mask_steps::mask_steps] = True
                
                mask_data.append(self.chip.write_masks(range(col[0],col[0]+col[1]), write=False))
            
        inj_data = self.chip.inject_analog_single(wait_cycles=100, write = False)
        
        mask_data_clean = []
        self.chip.reset_masks(tdac=False)
        for col in col_list:
            mask_data_clean.append(self.chip.write_masks(range(col[0],col[0]+col[1]), write=False))
            
        return inj_data, mask_data, mask_data_clean
    

    def start(self, **kwargs):
        self.chip.init()
        
        self._first_read = False
        self.scan_param_id = 0
        self.fifo_readout = FifoReadout(self.chip)
        
        self.chip.init_communication()
        
        self.chip.reset_chip()
        
        filename = self.output_filename + '.h5'
        filter_raw_data = tb.Filters(complib='blosc', complevel=5, fletcher32=False)
        self.filter_tables = tb.Filters(complib='zlib', complevel=5, fletcher32=False)
        self.h5_file = tb.open_file(filename, mode='w', title=self.scan_id)
        self.raw_data_earray = self.h5_file.create_earray(self.h5_file.root, name='raw_data', atom=tb.UIntAtom(),
                                                         shape=(0,), title='raw_data', filters=filter_raw_data)
        self.meta_data_table = self.h5_file.create_table(self.h5_file.root, name='meta_data', description=MetaTable,
                                                        title='meta_data', filters=self.filter_tables)
        
        self.meta_data_table.attrs.run_name = self.run_name
        self.meta_data_table.attrs.scan_id = self.scan_id
        self.meta_data_table.attrs.kwargs = yaml.dump(kwargs)
        self.meta_data_table.attrs.dacs = yaml.dump(self.chip.dacs)
        self.meta_data_table.attrs.software_version = get_software_version()

        # Setup data sending
        socket_addr = kwargs.pop('send_data', None)
        if socket_addr:
            self.logger.info('Send data to server %s', socket_addr)
            self.context = zmq.Context()
            self.socket = self.context.socket(zmq.PUB)  # publisher socket
            self.socket.bind(socket_addr)
        else:
            self.socket = None

        self.scan(**kwargs)

        self.fifo_readout.print_readout_status()

        # Read all important chip values and dump to yaml
        self.meta_data_table.attrs.dac_status = self.chip.get_chip_status()

        self.h5_file.close()
        self.logger.info('Data Output Filename: %s', self.output_filename + '.h5')

        if self.socket:
            self.logger.info('Closing socket connection')
            self.socket.close()
            self.socket = None

        self.logger.removeHandler(self.fh)
        self.chip['cmd'].reset()


    def analyze(self):
        raise NotImplementedError('ScanBase.analyze() not implemented')


    def scan(self, **kwargs):
        raise NotImplementedError('ScanBase.scan() not implemented')


    @contextmanager
    def readout(self, *args, **kwargs):
        timeout = kwargs.pop('timeout', 10.0)

        # self.fifo_readout.readout_interval = 10
        if not self._first_read:
            self.fifo_readout.reset_rx()
            time.sleep(0.1)
            self.fifo_readout.print_readout_status()
            self._first_read = True

        self.start_readout(*args, **kwargs)
        yield
        self.fifo_readout.stop(timeout=timeout)


    def start_readout(self, scan_param_id=0, *args, **kwargs):
        # Pop parameters for fifo_readout.start
        callback = kwargs.pop('callback', self.handle_data)
        clear_buffer = kwargs.pop('clear_buffer', False)
        fill_buffer = kwargs.pop('fill_buffer', False)
        reset_sram_fifo = kwargs.pop('reset_sram_fifo', True)
        errback = kwargs.pop('errback', self.handle_err)
        no_data_timeout = kwargs.pop('no_data_timeout', None)
        self.scan_param_id = scan_param_id
        self.fifo_readout.start(reset_sram_fifo=reset_sram_fifo, fill_buffer=fill_buffer, clear_buffer=clear_buffer,
                                callback=callback, errback=errback, no_data_timeout=no_data_timeout)


    def handle_data(self, data_tuple):
        '''
            Handling of the data.
        '''
#         get_bin = lambda x, n: format(x, 'b').zfill(n)

        total_words = self.raw_data_earray.nrows

        self.raw_data_earray.append(data_tuple[0])
        self.raw_data_earray.flush()

        len_raw_data = data_tuple[0].shape[0]
        self.meta_data_table.row['timestamp_start'] = data_tuple[1]
        self.meta_data_table.row['timestamp_stop'] = data_tuple[2]
        self.meta_data_table.row['error'] = data_tuple[3]
        self.meta_data_table.row['data_length'] = len_raw_data
        self.meta_data_table.row['index_start'] = total_words
        total_words += len_raw_data
        self.meta_data_table.row['index_stop'] = total_words
        self.meta_data_table.row['scan_param_id'] = self.scan_param_id
        
#         # TODO: Remove? What was this for? It's very slow...
#         counter = 0
#         for _ in data_tuple[0]:
#             counter = counter + int(get_bin(int(data_tuple[0][0]), 32)[1])
#         self.meta_data_table.row['trigger'] = counter / len(data_tuple[0])
        
        self.meta_data_table.row.append()
        self.meta_data_table.flush()

        if self.socket:
            send_data(self.socket, data=data_tuple, scan_par_id=self.scan_param_id)


    def handle_err(self, exc):
        msg = '%s' % exc[1]
        if msg:
            self.logger.error('%s%s Aborting run...', msg, msg[-1])
        else:
            self.logger.error('Aborting run...')