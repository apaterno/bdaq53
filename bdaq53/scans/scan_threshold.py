#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import yaml
from tqdm import tqdm
import numpy as np

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ThresholdScan(ScanBase):
    scan_id = "threshold_scan"
    
    def scan(self, mask_steps=4, n_injections=100, start_column=0, stop_column=400, column_step=8,
            VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, **kwargs):
        '''Threshold scan loop

        Parameters
        ----------
        mask_steps : int
            Number of mask steps.
        n_injections : int
            Number of injections.
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        column_step : int
            Number of columns to inject to to at once.
            
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan.
        VCAL_HIGH_step : int
            VCAL_HIGH interval.
        
        '''

        self.chip.reset_chip()
        
        self.chip.set_dacs(**kwargs)
        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)

        columns = range(start_column, stop_column)
        col_list = [[e[0], len(e)] for e in [columns[i:i + column_step] for i in xrange(0, len(columns), column_step)]]
        
        inj_data, mask_data, mask_data_clean = self.prepare_masks(col_list, mask_steps)
        
        logger.info('Starting scan.')
        pbar = tqdm(total=(len(vcal_high_range)*len(col_list))*mask_steps)
        for scan_param_id, vcal_high in enumerate(vcal_high_range):
            self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED)
            
            with self.readout(scan_param_id=scan_param_id):
                mask_inx = 0
                for col_inx, _ in enumerate(col_list):
                    for _ in range(mask_steps):
                        for indata in mask_data[mask_inx]:
                            self.chip.write_command(indata)
                        self.chip.write_command(inj_data, repetitions=n_injections)
                        mask_inx += 1
                        pbar.update(1)
                    for indata in mask_data_clean[col_inx]:
                        self.chip.write_command(indata) # TODO: This can be done faster with brodcast?
        pbar.close()
            
    def analyze(self):
        with analysis.AnalyzeRawData(raw_data_file=self.output_filename + '.h5') as a:
            logger.info('Analyzing data...')
            a.analyze_data()
            VCAL_HIGH_opt = np.median(a.threshold_map[np.nonzero(a.threshold_map)])
            
            logger.info('Creating selected plots...')
            a.create_parameter_page()
            a.create_occupancy_map()
            a.create_tot_plot()
            a.create_rel_bcid_plot()
            a.create_scurves_plot()
            a.create_threshold_plot()
            a.create_threshold_map()
            a.create_noise_plot(scan_parameter_name='$\Delta VCAL')
            a.create_noise_map()
            if a.cluster_hits:
                a.create_cluster_size_plot()
                a.create_cluster_tot_plot()
                a.create_cluster_shape_plot()
                
        return VCAL_HIGH_opt


if __name__ == "__main__":
    with open('scan_threshold.yaml', 'r') as f:
        configuration = yaml.load(f)
    
    scan = ThresholdScan()
    scan.start(**configuration)
    scan.analyze()
