#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import yaml
import numpy as np
from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def set_logo_mask(self, image='../logo.bmp'):
    from pylab import imread
    
    img = np.invert(imread(image).astype('bool'))
    
    self.chip.enable_mask[105:295, 1:191] = np.transpose(img[:, :, 0])
    self.chip.injection_mask[105:295, 1:191] = np.transpose(img[:, :, 0])
    
    self.chip.enable_mask[:,0] = False
    self.chip.injection_mask[:,0] = False
    self.chip.enable_mask[:,-1] = False
    self.chip.injection_mask[:,-1] = False
    self.chip.enable_mask[0:105,:] = False
    self.chip.injection_mask[0:105,:] = False
    self.chip.enable_mask[295:400,:] = False
    self.chip.injection_mask[295:400,:] = False



class DigitalScan(ScanBase):
    scan_id = "digital_scan"

    def scan(self, mask_steps=4, n_injections=100, start_column=0, stop_column=400, column_step=8, **kwargs):
        '''Digital scan loop

        Parameters
        ----------
        mask_steps : int
            Number of mask steps.
        n_injections : int
            Number of injections.
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        column_step : int
            Number of columns to inject to to at once.
        '''
            
        logger.info('Starting Digital Scan')
        self.chip.reset_chip()

        columns = range(start_column, stop_column)
        col_list = [[e[0], len(e)] for e in [columns[i:i + column_step] for i in xrange(0, len(columns), column_step)]]
        
                
        with self.readout():
            pbar = tqdm(total=len(col_list)*mask_steps)
            for col in col_list:
                for m in range(mask_steps):
                    self.chip.reset_masks()
                    self.chip.enable_mask[col[0]:col[0]+col[1], :] = True
                    
                    #set_logo_mask(self)
                    
                    self.chip.injection_mask[col[0]:col[0]+col[1],m::mask_steps] = True
                    self.chip.write_masks(range(col[0],col[0]+col[1]))
                    
                    self.chip.inject_digital(repetitions=n_injections)
                    pbar.update(1)
                    
                self.chip.reset_masks()
                self.chip.write_masks(range(col[0],col[0]+col[1]))
            
            pbar.close()

    def analyze(self):
        with analysis.AnalyzeRawData(raw_data_file=self.output_filename + '.h5') as a:
            logger.info('Analyzing data...')
            a.analyze_data()
            logger.info('Creating selected plots...')
            a.create_parameter_page()
            a.create_occupancy_map()
            a.create_tot_plot()
            a.create_rel_bcid_plot()
            if a.cluster_hits:
                a.create_cluster_size_plot()
                a.create_cluster_tot_plot()
                a.create_cluster_shape_plot()


if __name__ == "__main__":
    with open('scan_digital.yaml', 'r') as f:
        configuration = yaml.load(f)
    
    scan = DigitalScan()
    scan.start(**configuration)
    scan.analyze()
