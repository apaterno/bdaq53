#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import numpy as np
from tqdm import tqdm

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis


local_configuration = {
    'mask_steps': 4,
    'n_injections': 100,
    
    'start_column': 160,
    'stop_column': 168,

    'REF_KRUM_LIN': 200,
    
    'VCAL_MED': 500,
    'VCAL_HIGH': 2000,
    
    'Vthreshold_LIN': 420,

    'LDAC_LIN': 130,
    }


class LocalThresholdTuning(ScanBase):
    scan_id = 'local_threshold_tuning'
    
    
    def prepare_masks(self, col_list, mask_steps):
        self.logger.info('Preparing masks...')
        mask_data = []
        for tdac in range(16):
            for col in col_list:
                for m in range(mask_steps):
                    self.chip.reset_masks(tdac=False)
                    self.chip.enable_mask[col[0]:col[0]+col[1],:] = True
                    self.chip.tdac_mask[:,:] = tdac
                    
                    # some magic to enable injection not in the same island 
                    self.chip.injection_mask[col[0]:col[0]+col[1]:2, m::mask_steps] = True
                    self.chip.injection_mask[col[0]+1:col[0]+col[1]:2, (m+2)%mask_steps::mask_steps] = True
                    
                    mask_data.append(self.chip.write_masks(range(col[0],col[0]+col[1]), write=False))
            
        inj_data = self.chip.inject_analog_single(wait_cycles=100, write = False) # TODO: should optimize wait_cycles?
        
        mask_data_clean = []
        self.chip.reset_masks(tdac=False)
        for col in col_list:
            mask_data_clean.append(self.chip.write_masks(range(col[0],col[0]+col[1]), write=False))
            
        return inj_data, mask_data, mask_data_clean
    
    
    def scan(self, mask_steps=4, n_injections=100, start_column=0, stop_column=400, column_step=8,
             VCAL_MED=500, VCAL_HIGH=4000, **kwargs):
        
        columns = range(start_column, stop_column)
        col_list = [[e[0], len(e)] for e in [columns[i:i + column_step] for i in xrange(0, len(columns), column_step)]]
        
        self.chip.set_dacs(**kwargs)
        inj_data, mask_data, mask_data_clean = self.prepare_masks(col_list, mask_steps)
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)
        
        logger.info('Starting TDAC tuning')
        pbar = tqdm(total=(16*len(col_list))*mask_steps)
        mask_inx = 0
        for scan_param_id in range(16):
            with self.readout(scan_param_id=scan_param_id):
                for col_inx, _ in enumerate(col_list):
                    for _ in range(mask_steps):
                        for indata in mask_data[mask_inx]:
                            self.chip.write_command(indata)
                        self.chip.write_command(inj_data, repetitions=n_injections)
                        mask_inx += 1
                        pbar.update(1)
                    for indata in mask_data_clean[col_inx]:
                        self.chip.write_command(indata) # TODO: This can be done faster with brodcast?
        pbar.close()
        
    def analyze(self):
        with analysis.AnalyzeRawData(raw_data_file=self.output_filename + '.h5') as a:
            logger.info('Analyzing data...')
            a.analyze_data()
            
            logger.info('Creating selected plots...')
            a.create_parameter_page()
            a.create_occupancy_map()
            a.create_tot_plot()
            a.create_rel_bcid_plot()
            a.create_scurves_plot(scan_parameter_name='TDAC')
            a.create_threshold_plot(scan_parameter_name='TDAC')
            a.create_threshold_map()
            a.create_noise_plot(scan_parameter_name='TDAC')
            a.create_noise_map()
            if a.cluster_hits:
                a.create_cluster_size_plot()
                a.create_cluster_tot_plot()
                a.create_cluster_shape_plot()
                
        return a.threshold_map
    


if __name__ == "__main__":
    tuning = LocalThresholdTuning()
    tuning.start(**local_configuration)
    print tuning.analyze()