#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import yaml
from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class AnalogScan(ScanBase):
    scan_id = "analog_scan"

    def scan(self, mask_steps=4, n_injections=100, start_column=0, stop_column=400, column_step=8, **kwargs):
        '''Analog scan loop

        Parameters
        ----------
        mask_steps : int
            Number of mask steps.
        n_injections : int
            Number of injections.
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        column_step : int
            Number of columns to inject to to at once.
        '''

        logger.info('Starting Analog Scan')
        self.chip.reset_chip()
        
        self.chip.set_dacs(**kwargs)
        self.chip.setup_analog_injection(vcal_high=kwargs.get('VCAL_HIGH', 4000), vcal_med=kwargs.get('VCAL_MED', 1000))
        
        columns = range(start_column, stop_column)
        col_list = [[e[0], len(e)] for e in [columns[i:i + column_step] for i in xrange(0, len(columns), column_step)]]

                        
        with self.readout():
            pbar = tqdm(total=len(col_list)*mask_steps)
            for col in col_list:
                for m in range(mask_steps):
                    self.chip.reset_masks(tdac=False)
                    self.chip.enable_mask[col[0]:col[0]+col[1],m::mask_steps] = True
                    self.chip.injection_mask[col[0]:col[0]+col[1],m::mask_steps] = True
                    self.chip.write_masks(range(col[0],col[0]+col[1]))
                    
                    self.chip.inject_analog_single(repetitions=n_injections)
                    pbar.update(1)
                    
                self.chip.reset_masks(tdac=False)
                self.chip.write_masks(range(col[0],col[0]+col[1]))
            pbar.close()
        
    def analyze(self):
        with analysis.AnalyzeRawData(raw_data_file=self.output_filename + '.h5') as a:
            logger.info('Analyzing data...')
            a.analyze_data()
            logger.info('Creating selected plots...')
            a.create_parameter_page()
            a.create_occupancy_map()
            a.create_tot_plot()
            a.create_rel_bcid_plot()
            if a.cluster_hits:
                a.create_cluster_size_plot()
                a.create_cluster_tot_plot()
                a.create_cluster_shape_plot()


if __name__ == "__main__":
    with open('scan_analog.yaml', 'r') as f:
        configuration = yaml.load(f)
        
    scan = AnalogScan()
    scan.start(**configuration)
    scan.analyze()
