#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import logging
import yaml
import numpy as np
from tqdm import tqdm

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis


local_configuration = {
    'mask_steps': 4,
    'n_injections': 100,
    
    'start_column': 160,
    'stop_column': 168,

    'REF_KRUM_LIN': 200,
    
    'VCAL_MED': 500,
    'VCAL_HIGH': 2000,
    
    'Vthreshold_LIN_start': 350,    # keep above 450
    'Vthreshold_LIN_stop': 500,
    'Vthreshold_LIN_step': 2,
    
    'TDAC': 7,
    'LDAC_LIN': 150,
    }


class GlobalThresholdTuning(ScanBase):
    scan_id = 'global_threshold_tuning'

    def scan(self, mask_steps=4, n_injections=100, start_column=0, stop_column=400, column_step=8,
             VCAL_MED=500, VCAL_HIGH=4000, Vthreshold_LIN_start=0, Vthreshold_LIN_stop=1024, Vthreshold_LIN_step=1, TDAC=7, **kwargs):
        
        columns = range(start_column, stop_column)
        col_list = [[e[0], len(e)] for e in [columns[i:i + column_step] for i in xrange(0, len(columns), column_step)]]
        
        
        kwargs['TDAC'] = TDAC
        self.chip.set_dacs(**kwargs)
        inj_data, mask_data, mask_data_clean = self.prepare_masks(col_list, mask_steps)
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)
        
        logger.info('Starting global threshold tuning')
        vth_range = range(Vthreshold_LIN_start, Vthreshold_LIN_stop, Vthreshold_LIN_step)
        pbar = tqdm(total=(len(vth_range)*len(col_list))*mask_steps)
        for scan_param_id, vth in enumerate(vth_range):
            self.chip.write_register('Vthreshold_LIN', vth)
            with self.readout(scan_param_id=scan_param_id):
                mask_inx = 0
                for col_inx, _ in enumerate(col_list):
                    for _ in range(mask_steps):
                        for indata in mask_data[mask_inx]:
                            self.chip.write_command(indata)
                        self.chip.write_command(inj_data, repetitions=n_injections)
                        mask_inx += 1
                        pbar.update(1)
                    for indata in mask_data_clean[col_inx]:
                        self.chip.write_command(indata) # TODO: This can be done faster with brodcast?
        pbar.close()
                        
    
    def analyze(self):
        with analysis.AnalyzeRawData(raw_data_file=self.output_filename + '.h5') as a:
            logger.info('Analyzing data...')
            a.analyze_data()
            
            vth_opt = np.median(a.threshold_map[np.nonzero(a.threshold_map)])
            
            logger.info('Creating selected plots...')
            a.create_parameter_page()
            a.create_occupancy_map()
            a.create_tot_plot()
            a.create_rel_bcid_plot()
            a.create_scurves_plot(scan_parameter_name='Vthreshold_LIN')
            a.create_threshold_plot(scan_parameter_name='Vthreshold_LIN')
            a.create_threshold_map()
            a.create_noise_plot(scan_parameter_name='Vthreshold_LIN')
            a.create_noise_map()
            if a.cluster_hits:
                a.create_cluster_size_plot()
                a.create_cluster_tot_plot()
                a.create_cluster_shape_plot()
                
        return vth_opt
    


if __name__ == "__main__":
    tuning = GlobalThresholdTuning()
    tuning.start(**local_configuration)
    print tuning.analyze()