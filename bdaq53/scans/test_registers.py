#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import random
import logging
import time

from bdaq53.rd53a import rd53a
from bdaq53.register_utils import RD53ARegisterParser

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


local_configuration = {'addresses': [1, 2, 3, 4, 5, 6, 7, 8, 9,10,
                                    11,12,13,14,15,16,17,18,19,20,
                                    21,22,23,24,25,26,27,28,29,30,
                                    31,32,33   ,35   ,37,38,39,
                                    41,42,43,44   ,46,47,48,49,50,
                                    51,52,53,54,55,56,57,58,59
                                      ,62,
                                                   76,77,78,79,80,
                                    81,82,83,84,85,86,87,88,89,90,
                                    91,92,93,94,95,96,97,98,99,100,
                                        105,106,107,108,109,135]}

class RegisterTest(object):
    rw_registers = [1, 2, 3, 4, 5, 6, 7, 8, 9,10,
                   11,12,13,14,15,16,17,18,19,20,
                   21,22,23,24,25,26,27,28,29,30,
                   31,32,33   ,35   ,37,38,39,
                   41,42,43,44   ,46,47,48,49,50,
                   51,52,53,54,55,56,57,58,59
                     ,62,
                                  76,77,78,79,80,
                   81,82,83,84,85,86,87,88,89,90,
                   91,92,93,94,95,96,97,98,99,100,
                          105,106,107,108,109,135]

    def __init__(self, dut_conf=None):
        self.chip = rd53a(dut_conf)
        self.rp = RD53ARegisterParser()

    def start(self, **kwargs):
        self.chip.init()
        self.chip.init_communication()
        self.chip.reset_chip()

        # USER_K message type filter
        self.chip['rx'].set_USER_K_FILTER_MASK_1(0x01)  # only allow frames containing monitor data
        self.chip['rx'].set_USER_K_FILTER_MASK_2(0x02)  # only allow frames containing monitor data
        self.chip['rx'].set_USER_K_FILTER_MASK_3(0x04)  # only allow frames containing monitor data
        self.chip['rx'].set_USER_K_FILTER_EN(True)      # enable the filter
        logger.info('USER_K filter enabled')

        # Enable Monitor Data output
        indata = self.chip.write_register(register='GLOBAL_PULSE_ROUTE', data=0x0100, write=False)
        indata += self.chip.write_global_pulse(width = 4, write=False)
        self.chip.write_command(indata)

        for _ in range(300):
            self.chip['rx'].get_rx_ready()

        self.addresses = kwargs.pop('addresses')
        
        self.test()


    def test(self, **kwargs):
        self.values = {}
        self.results = {}

        #self.chip.get_chip_status()
        for address in self.addresses:
            if not address in self.rw_registers:
                raise ValueError('Register at address %i is not a writable register!' % (address))

            size = self.rp.get_size(address)
            value = int(random.getrandbits(size))
            self.values[address] = value

            logger.debug('Writing random data %s to register %s at address %s' % (bin(value), self.rp.get_name(address), hex(address)))
            self.chip.write_register(register=address, data=value, write=True)
            self.chip['FIFO'].get_data()
            self.chip.read_register(register=address, write=True)

            for _ in range(1000):
                if self.chip['FIFO'].get_FIFO_SIZE() > 0:
                    userk_data = self.chip.proc_userk(self.chip.interpret_userk_data(self.chip['FIFO'].get_data()))
                    if len(userk_data) > 0:
                        break
            else:
                raise Exception('Timeout while reading register data!')

            self.results[userk_data[-1][0]] = userk_data[-1][2]
        self.chip.write_ecr()






    def evaluate(self):
        for address in self.addresses:
            if self.results[address] != self.values[address]:
                logger.error('Register %s at address %s read back a wrong value!' % (self.rp.get_name(address), hex(address)))
                raise ValueError('Register %s at address %s read back a wrong value!' % (self.rp.get_name(address), hex(address)))

        logger.info('Success!')

    def get_chip(self):
        return self.chip


if __name__ == "__main__":
    rt = RegisterTest()
    rt.start(**local_configuration)
    rt.evaluate()