#!/usr/bin/env python

from setuptools import setup
from setuptools import find_packages

import bdaq53 

version = bdaq53.__version__

author = 'Michael Daas, Tomasz Hemperek, Mark Standke, Marco Vogt'
author_email = ''

# Requirements
install_requires = ['basil-daq==2.4.10', 'bitarray>=0.8.1', 'matplotlib',
                    'numpy', 'online_monitor==0.3.1',
                    'pixel_clusterizer==3.1.3', 'tables', 'pyyaml', 'pyzmq',
                    'scipy', 'numba', 'tqdm']

setup(
    name='bdaq53',
    version=version,
    description='DAQ for RD53A prototype',
    url='https://gitlab.cern.ch/silab/bdaq53',
    license='',
    long_description='',
    author=author,
    maintainer=author,
    author_email=author_email,
    maintainer_email=author_email,
    install_requires=install_requires,
    packages=find_packages(),
    include_package_data=True,
    platforms='any',
    entry_points={
    'console_scripts': [
        'bdaq53 = bdaq53.bdaq53:main',
    ]
},
)
