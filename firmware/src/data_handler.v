`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/18/2017 12:15:19 PM
// Design Name: 
// Module Name: data_handler
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module data_handler(
    input wire        BUS_CLK,
    input wire        RESET_N,
    
    input wire [7:0]    GPIO_IO,
    output wire [7:0]   LED,
    
    input wire          TCP_RX_WR,
    input wire          TCP_CLOSE_REQ,
    output wire [10:0]  TCP_RX_WC_11B,
    
    
    input wire          FIFO_FULL, 
    output wire         fifo_write,
    output wire [31:0]  fifo_data_in,
    
    // Slave AXI stream ports
    output wire         s_axis_tvalid,
    input wire          s_axis_tready,
    output wire [31:0]  s_axis_tdata,
    output wire         s_axis_tlast,
    input wire          vfifo_s2mm_channel_full,
   
    // Master AXI stream ports
    input wire          m_axis_tvalid,
    output wire         m_axis_tready,
    input wire [31:0]   m_axis_tdata,
    input wire          m_axis_tlast,
    input wire          vfifo_mm2s_channel_empty
    );
    
    
    reg ETH_START_SENDING, ETH_START_SENDING_temp, ETH_START_SENDING_LOCK;
    reg [31:0] datasource_int;   
    reg [10:0] TCP_RX_WC_11B_int;    
    reg fifo_write_int;
    reg [31:0] fifo_data_in_int;
        
    assign TCP_RX_WC_11B = TCP_RX_WC_11B_int;
    assign fifo_write = fifo_write_int;
    assign fifo_data_in = fifo_data_in_int;
    
    reg s_axis_tvalid_int, s_axis_tlast_int, m_axis_tready_int;
    reg [31:0] s_axis_tdata_int;
    assign s_axis_tvalid = s_axis_tvalid_int;
    assign s_axis_tlast = s_axis_tlast_int;
    assign m_axis_tready = m_axis_tready_int;
    assign s_axis_tdata = s_axis_tdata_int;
    
    
    always@ (posedge BUS_CLK) 
    begin
        if (!RESET_N) begin
            s_axis_tvalid_int <= 1'b0;
            s_axis_tlast_int <= 1'b0;
            m_axis_tready_int <= 1'b0;
            datasource_int <= 32'd0;
        end
        else
            begin
            // wait for start condition
            ETH_START_SENDING <= GPIO_IO[0];    //TCP_OPEN_ACK;
            
            if(ETH_START_SENDING && !ETH_START_SENDING_temp)
                ETH_START_SENDING_LOCK <= 1;
            ETH_START_SENDING_temp <= ETH_START_SENDING;  
            
            // RX FIFO word counter
            if(TCP_RX_WR) begin
                TCP_RX_WC_11B_int <= TCP_RX_WC_11B_int + 1;
            end
            else begin
                TCP_RX_WC_11B_int <= 11'd0;
            end
        
            // Fill DDR FIFO with counter data
            if (s_axis_tready && !vfifo_s2mm_channel_full) begin
                s_axis_tvalid_int <= 1'b1;
                s_axis_tdata_int <= datasource_int;
                datasource_int <= datasource_int + 1;
            end
            else
                s_axis_tvalid_int <= 1'b0;
        
            // FIFO handshake
            if(ETH_START_SENDING_LOCK) begin
                if(!FIFO_FULL) begin
                    m_axis_tready_int <= 1'b1;
                    if (m_axis_tvalid && !vfifo_mm2s_channel_empty) begin
                        fifo_data_in_int <= m_axis_tdata;
                        fifo_write_int <= 1'b1;
                    end
                end
                else begin
                    m_axis_tready_int <= 1'b1;
                    fifo_write_int <= 1'b0;
                end
            end
        
            // stop, if connection is closed by host
            if(TCP_CLOSE_REQ || !GPIO_IO[0]) begin
                ETH_START_SENDING_LOCK <= 0;
                fifo_write_int <= 1'b0;
                datasource_int <= 32'd0;
            end
 
        end
    end    
    
endmodule
