
# ------------------------------------------------------------
#  Copyright (c) SILAB , Physics Institute of Bonn University
# ------------------------------------------------------------

#
#   Constraints for the MMC hardware with the KX1(160T) FPGA board
#

create_clock -period 10.000 -name clkin -add [get_ports clkin]
create_clock -period 8.000 -name rgmii_rxc -add [get_ports rgmii_rxc]
create_clock -period 5.000 -name CLK200_P -add [get_ports CLK200_P]
create_clock -period 7.813 -name MGT_REFCLK0_P -add [get_ports MGT_REFCLK0_P]
create_clock -period 25.000 [get_pins -hier -filter name=~*aurora_64b66b_0_wrapper_i*aurora_64b66b_0_multi_gt_i*aurora_64b66b_0_gtx_inst/gtxe2_i/RXOUTCLK]
create_clock -period 25.000 [get_pins -hier -filter name=~*aurora_64b66b_0_wrapper_i*aurora_64b66b_0_multi_gt_i*aurora_64b66b_0_gtx_inst/gtxe2_i/TXOUTCLK]

create_generated_clock -name i_bdaq53_core/i_clock_divisor_i2c/I2C_CLK -source [get_pins PLLE2_BASE_inst/CLKOUT0] -divide_by 1500 [get_pins i_bdaq53_core/i_clock_divisor_i2c/CLOCK_reg/Q]
create_generated_clock -name rgmii_txc -source [get_pins rgmii/ODDR_inst/C] -divide_by 1 [get_ports rgmii_txc]

set_false_path -from [get_clocks CLK125PLLTX] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK125PLLTX]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks rgmii_rxc]
set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks CLK200_P] -to [get_clocks MGT_REFCLK0_P]
set_false_path -from [get_clocks MGT_REFCLK0_P] -to [get_clocks CLK200_P]
set_false_path -from [get_clocks i_bdaq53_core/i_clock_divisor_i2c/I2C_CLK] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks i_bdaq53_core/i_clock_divisor_i2c/I2C_CLK]
set_false_path -from [get_clocks -of_objects [get_pins i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_0_block_i/clock_module_i/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_0_block_i/clock_module_i/mmcm_adv_inst/CLKOUT0]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks CLK200_P]
set_false_path -from [get_clocks CLK200_P] -to [get_clocks -of_objects [get_pins i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_0_block_i/clock_module_i/mmcm_adv_inst/CLKOUT0]]

set_false_path -from [get_clocks i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_0_block_i/aurora_64b66b_0_i/inst/aurora_64b66b_0_wrapper_i/aurora_64b66b_0_multi_gt_i/aurora_64b66b_0_gtx_inst/gtxe2_i/RXOUTCLK] -to [get_clocks CLK200_P]
set_false_path -from [get_clocks i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_0_block_i/aurora_64b66b_0_i/inst/aurora_64b66b_0_wrapper_i/aurora_64b66b_0_multi_gt_i/aurora_64b66b_0_gtx_inst/gtxe2_i/RXOUTCLK] -to [get_clocks user_clk_i]

set_property ASYNC_REG true [get_cells sitcp/SiTCP/GMII/GMII_TXCNT/irMacPauseExe_0]
set_property ASYNC_REG true [get_cells sitcp/SiTCP/GMII/GMII_TXCNT/irMacPauseExe_1]

set_clock_groups -asynchronous -group [get_clocks rgmii_rxc] -group [get_clocks CLK125PLLTX]
set_clock_groups -asynchronous -group [get_clocks user_clk_i] -group [get_clocks CLK200_P]


#Oscillator 100MHz
set_property PACKAGE_PIN AA3 [get_ports clkin]
set_property IOSTANDARD LVCMOS15 [get_ports clkin]

#Oscillator 200MHz
set_property PACKAGE_PIN AD18 [get_ports CLK200_N]
set_property PACKAGE_PIN AC18 [get_ports CLK200_P]
set_property IOSTANDARD LVDS [get_ports CLK200_*]

#CLK Mux
set_property PACKAGE_PIN K25 [get_ports CLK_SEL]
set_property IOSTANDARD LVCMOS33 [get_ports CLK_SEL]
set_property PULLUP true [get_ports CLK_SEL]


# Reset push button
set_property PACKAGE_PIN C18 [get_ports RESET_BUTTON]
set_property IOSTANDARD LVCMOS33 [get_ports RESET_BUTTON]
set_property PULLUP true [get_ports RESET_BUTTON]


#SITCP
set_property SLEW FAST [get_ports mdio_phy_mdc]
set_property IOSTANDARD LVCMOS33 [get_ports mdio_phy_mdc]
set_property PACKAGE_PIN N16 [get_ports mdio_phy_mdc]

set_property SLEW FAST [get_ports mdio_phy_mdio]
set_property IOSTANDARD LVCMOS33 [get_ports mdio_phy_mdio]
set_property PACKAGE_PIN U16 [get_ports mdio_phy_mdio]

set_property SLEW FAST [get_ports phy_rst_n]
set_property IOSTANDARD LVCMOS33 [get_ports phy_rst_n]
set_property PACKAGE_PIN M20 [get_ports phy_rst_n]

set_property IOSTANDARD LVCMOS33 [get_ports rgmii_rxc]
set_property PACKAGE_PIN R21 [get_ports rgmii_rxc]

set_property IOSTANDARD LVCMOS33 [get_ports rgmii_rx_ctl]
set_property PACKAGE_PIN P21 [get_ports rgmii_rx_ctl]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_rxd[0]}]
set_property PACKAGE_PIN P16 [get_ports {rgmii_rxd[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_rxd[1]}]
set_property PACKAGE_PIN N17 [get_ports {rgmii_rxd[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_rxd[2]}]
set_property PACKAGE_PIN R16 [get_ports {rgmii_rxd[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_rxd[3]}]
set_property PACKAGE_PIN R17 [get_ports {rgmii_rxd[3]}]

set_property SLEW FAST [get_ports rgmii_txc]
set_property IOSTANDARD LVCMOS33 [get_ports rgmii_txc]
set_property PACKAGE_PIN R18 [get_ports rgmii_txc]

set_property SLEW FAST [get_ports rgmii_tx_ctl]
set_property IOSTANDARD LVCMOS33 [get_ports rgmii_tx_ctl]
set_property PACKAGE_PIN P18 [get_ports rgmii_tx_ctl]

set_property SLEW FAST [get_ports {rgmii_txd[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_txd[0]}]
set_property PACKAGE_PIN N18 [get_ports {rgmii_txd[0]}]
set_property SLEW FAST [get_ports {rgmii_txd[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_txd[1]}]
set_property PACKAGE_PIN M19 [get_ports {rgmii_txd[1]}]
set_property SLEW FAST [get_ports {rgmii_txd[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_txd[2]}]
set_property PACKAGE_PIN U17 [get_ports {rgmii_txd[2]}]
set_property SLEW FAST [get_ports {rgmii_txd[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_txd[3]}]
set_property PACKAGE_PIN T17 [get_ports {rgmii_txd[3]}]


# Aurora related signals
set_property PACKAGE_PIN D5 [get_ports MGT_REFCLK0_N]
set_property PACKAGE_PIN D6 [get_ports MGT_REFCLK0_P]
#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK0_N]
#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK0_P]
#set_property PACKAGE_PIN F6 [get_ports MGT_REFCLK1_P]
#set_property PACKAGE_PIN F5 [get_ports MGT_REFCLK1_N]
#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK1_N]
#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK1_P]

#set_property IOSTANDARD LVDS [get_ports RX_INIT_CLK_*]
#set_property DIFF_TERM false [get_ports RX_INIT_CLK_*]
#set_property PACKAGE_PIN AC18 [get_ports RX_INIT_CLK_P]
#set_property PACKAGE_PIN AD18 [get_ports RX_INIT_CLK_N]

#set_property IOSTANDARD LVDS [get_ports MGT_RX_*]
set_property PACKAGE_PIN G4 [get_ports {MGT_RX_P[0]}]
set_property PACKAGE_PIN G3 [get_ports {MGT_RX_N[0]}]
set_property PACKAGE_PIN E4 [get_ports {MGT_RX_P[1]}]
set_property PACKAGE_PIN E3 [get_ports {MGT_RX_N[1]}]
set_property PACKAGE_PIN C4 [get_ports {MGT_RX_P[2]}]
set_property PACKAGE_PIN C3 [get_ports {MGT_RX_N[2]}]
set_property PACKAGE_PIN B6 [get_ports {MGT_RX_P[3]}]
set_property PACKAGE_PIN B5 [get_ports {MGT_RX_N[3]}]


# Debug LEDs
set_property PACKAGE_PIN M17 [get_ports {LED[0]}]
set_property PACKAGE_PIN L18 [get_ports {LED[1]}]
set_property PACKAGE_PIN L17 [get_ports {LED[2]}]
set_property PACKAGE_PIN K18 [get_ports {LED[3]}]
set_property PACKAGE_PIN P26 [get_ports {LED[4]}]
set_property PACKAGE_PIN M25 [get_ports {LED[5]}]
set_property PACKAGE_PIN L25 [get_ports {LED[6]}]
set_property PACKAGE_PIN P23 [get_ports {LED[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports LED*]
set_property SLEW SLOW [get_ports LED*]


# CMD encoder CLK (40 pin connector, IO_2_[P,N] = IO_[2,3],  Pin[7,8], Mercury: B[84,86] = IO_B16_L16_[G12_P,F12_N])
#set_property PACKAGE_PIN G12 [get_ports CMD_CLK_P]
#set_property PACKAGE_PIN F12 [get_ports CMD_CLK_N]
# CMD encoder DATA (40 pin connector, IO_10_[P,N] = IO_[10,11], Pin[9,10], Mercury: B[72,74 = IO_B16_L7_[F9_P,F8_N])
#set_property PACKAGE_PIN F9 [get_ports CMD_DATA_P]
#set_property PACKAGE_PIN F8 [get_ports CMD_DATA_N]
#set_property IOSTANDARD LVDS [get_ports CMD_*]

set_property PACKAGE_PIN AB21 [get_ports LEMO_TX0]
set_property PACKAGE_PIN V23 [get_ports LEMO_TX1]
set_property IOSTANDARD LVCMOS33 [get_ports LEMO_TX*]
set_property SLEW FAST [get_ports LEMO_TX*]


# IO port
#set_property PACKAGE_PIN H12 [get_ports IO22]
#set_property PACKAGE_PIN H11 [get_ports IO23]
#set_property IOSTANDARD LVCMOS33 [get_ports IO*]


# I2C pins
set_property PACKAGE_PIN N24 [get_ports I2C_SCL]
set_property PACKAGE_PIN P24 [get_ports I2C_SDA]
set_property IOSTANDARD LVCMOS33 [get_ports I2C_*]
set_property SLEW SLOW [get_ports I2C_*]


# EEPROM (SPI for SiTCP)
# IO25=B53, IO23=B56, IO19=B62, IO17=B65
set_property PACKAGE_PIN G14 [get_ports EEPROM_CS]
set_property PACKAGE_PIN H11 [get_ports EEPROM_SK]
set_property PACKAGE_PIN D8 [get_ports EEPROM_DI]
set_property PACKAGE_PIN A8 [get_ports EEPROM_DO]
set_property IOSTANDARD LVCMOS33 [get_ports EEPROM_*]


set_false_path -from [get_clocks -of_objects [get_pins i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_0_block_i/clock_module_i/mmcm_adv_inst/CLKOUT0]] -to [get_clocks i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_0_block_i/aurora_64b66b_0_i/inst/aurora_64b66b_0_wrapper_i/aurora_64b66b_0_multi_gt_i/aurora_64b66b_0_gtx_inst/gtxe2_i/RXOUTCLK]
set_false_path -from [get_clocks CLK200_P] -to [get_clocks i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_0_block_i/aurora_64b66b_0_i/inst/aurora_64b66b_0_wrapper_i/aurora_64b66b_0_multi_gt_i/aurora_64b66b_0_gtx_inst/gtxe2_i/RXOUTCLK]


set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT5]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT5]]
