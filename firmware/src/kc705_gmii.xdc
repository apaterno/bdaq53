
# ------------------------------------------------------------
#  Copyright (c) SILAB , Physics Institute of Bonn University
# ------------------------------------------------------------

#
#   Constraints for the XILINX KC705 evaluation board
#

create_clock -period 8.000 -name gmii_rx_clk -add [get_ports gmii_rx_clk]
create_clock -period 5.000 -name CLK200_P -add [get_ports CLK200_P]
create_clock -period 6.40 -name CLKSi570 -add [get_ports CLKSi570_P]
#create_clock -period 6.250 -name SMA_MGT_REFCLK_P -waveform {0.000 3.125} [get_ports SMA_MGT_REFCLK_P]


create_clock -period 6.250 [get_pins -hier -filter name=~*aurora_64b66b_1lane_kc705_wrapper_i*aurora_64b66b_1lane_kc705_multi_gt_i*aurora_64b66b_1lane_kc705_gtx_inst/gtxe2_i/TXOUTCLK]
create_clock -period 6.250 [get_pins -hier -filter name=~*aurora_64b66b_1lane_kc705_wrapper_i*aurora_64b66b_1lane_kc705_multi_gt_i*aurora_64b66b_1lane_kc705_gtx_inst/gtxe2_i/RXOUTCLK]

create_clock -period 6.250 -name Si5324 -add [get_ports Si5324_P]
create_generated_clock -name i2c_clk -source [get_pins PLLE2_BASE_inst/CLKOUT0] -divide_by 1600 [get_pins i_bdaq53_core/i_clock_divisor_i2c/CLOCK_reg/Q]

set_false_path -from [get_clocks Si5324] -to [get_clocks CLKSi570]
set_false_path -from [get_clocks CLKSi570] -to [get_clocks Si5324]

set_false_path -from [get_clocks Si5324] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks Si5324]

set_false_path -from [get_clocks gmii_rx_clk] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks gmii_rx_clk]

set_false_path -from [get_clocks i2c_clk] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks i2c_clk]

set_false_path -from [get_clocks user_clk_i] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks user_clk_i]


set_false_path -from [get_clocks CLKSi570] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLKSi570]

set_false_path -from [get_clocks CLKSi570] -to [get_clocks user_clk_i]
set_false_path -from [get_clocks user_clk_i] -to [get_clocks CLKSi570]


#Oscillator 200MHz
set_property PACKAGE_PIN AD11 [get_ports CLK200_N]
set_property PACKAGE_PIN AD12 [get_ports CLK200_P]
set_property IOSTANDARD LVDS [get_ports CLK200_*]


#Oscillator Si570 (10...810 MHz, default = 156.25 MHz)
set_property PACKAGE_PIN K29 [get_ports CLKSi570_N]
set_property PACKAGE_PIN K28 [get_ports CLKSi570_P]
set_property IOSTANDARD LVDS_25 [get_ports CLKSi570_*]

#USER SMA CLOCK
#set_property PACKAGE_PIN K25 [get_ports USER_SMA_CLOCK_N]
#set_property IOSTANDARD LVDS_25 [get_ports USER_SMA_CLOCK_N]
#set_property PACKAGE_PIN L25 [get_ports USER_SMA_CLOCK_P]
#set_property IOSTANDARD LVDS_25 [get_ports USER_SMA_CLOCK_P]

#Oscillator  + jitter attenuator (MGT reference clock)
set_property PACKAGE_PIN L7 [get_ports Si5324_N]
set_property PACKAGE_PIN L8 [get_ports Si5324_P]
set_property PACKAGE_PIN AE20 [get_ports SI5324_RST]
set_property IOSTANDARD LVCMOS25 [get_ports SI5324_RST]


#Push buttons
set_property PACKAGE_PIN AB7 [get_ports RESET_BUTTON]
set_property IOSTANDARD LVCMOS15 [get_ports RESET_BUTTON]
set_property PACKAGE_PIN G12 [get_ports GPIO_SW_C]
set_property IOSTANDARD LVCMOS25 [get_ports GPIO_SW_C]
#set_property PACKAGE_PIN AG5 [get_ports GPIO_SW_E]
#set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_E]
#set_property PACKAGE_PIN AA12 [get_ports GPIO_SW_N]
#set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_N]
#set_property PACKAGE_PIN AB12 [get_ports GPIO_SW_S]
#set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_S]
#set_property PACKAGE_PIN AC6 [get_ports GPIO_SW_W]
#set_property IOSTANDARD LVCMOS15 [get_ports GPIO_SW_W]



#SITCP
set_property SLEW FAST [get_ports mdio_phy_mdc]
set_property PACKAGE_PIN R23 [get_ports mdio_phy_mdc]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdc]

set_property SLEW FAST [get_ports mdio_phy_mdio]
set_property IOSTANDARD LVCMOS25 [get_ports mdio_phy_mdio]
set_property PACKAGE_PIN J21 [get_ports mdio_phy_mdio]

set_property SLEW FAST [get_ports phy_rst_n]
set_property IOSTANDARD LVCMOS25 [get_ports phy_rst_n]
set_property PACKAGE_PIN L20 [get_ports phy_rst_n]


# GMII interface (KC705)
set_property PACKAGE_PIN R30 [get_ports gmii_crs]
set_property IOSTANDARD LVCMOS25 [get_ports gmii_crs]
set_property PACKAGE_PIN W19 [get_ports gmii_col]
set_property IOSTANDARD LVCMOS25 [get_ports gmii_col]

set_property PACKAGE_PIN U27 [get_ports gmii_rx_clk]
set_property PACKAGE_PIN R28 [get_ports gmii_rx_dv]
set_property PACKAGE_PIN V26 [get_ports gmii_rx_er]
set_property IOSTANDARD LVCMOS25 [get_ports gmii_rx_*]
set_property PACKAGE_PIN U30 [get_ports {gmii_rxd[0]}]
set_property PACKAGE_PIN U25 [get_ports {gmii_rxd[1]}]
set_property PACKAGE_PIN T25 [get_ports {gmii_rxd[2]}]
set_property PACKAGE_PIN U28 [get_ports {gmii_rxd[3]}]
set_property PACKAGE_PIN R19 [get_ports {gmii_rxd[4]}]
set_property PACKAGE_PIN T27 [get_ports {gmii_rxd[5]}]
set_property PACKAGE_PIN T26 [get_ports {gmii_rxd[6]}]
set_property PACKAGE_PIN T28 [get_ports {gmii_rxd[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {gmii_rxd[*]}]

set_property PACKAGE_PIN K30 [get_ports gmii_tx_clk]
set_property PACKAGE_PIN M27 [get_ports gmii_tx_en]
set_property PACKAGE_PIN N29 [get_ports gmii_tx_er]
set_property IOSTANDARD LVCMOS25 [get_ports gmii_tx_*]
set_property SLEW FAST [get_ports gmii_tx_*]
set_property PACKAGE_PIN N27 [get_ports {gmii_txd[0]}]
set_property PACKAGE_PIN N25 [get_ports {gmii_txd[1]}]
set_property PACKAGE_PIN M29 [get_ports {gmii_txd[2]}]
set_property PACKAGE_PIN L28 [get_ports {gmii_txd[3]}]
set_property PACKAGE_PIN J26 [get_ports {gmii_txd[4]}]
set_property PACKAGE_PIN K26 [get_ports {gmii_txd[5]}]
set_property PACKAGE_PIN L30 [get_ports {gmii_txd[6]}]
set_property PACKAGE_PIN J28 [get_ports {gmii_txd[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {gmii_txd[*]}]
set_property SLEW FAST [get_ports {gmii_txd[*]}]


# Aurora related signals: KC705 features 16 MGT pairs.
# SMA: MGT_BANK_117, GTXE2_CHANNEL_X0Y8

#set_property PACKAGE_PIN L7 [get_ports MGT_REFCLK0_N]
#set_property PACKAGE_PIN L8 [get_ports MGT_REFCLK0_P]

set_property PACKAGE_PIN J7 [get_ports SMA_MGT_REFCLK_N]
set_property PACKAGE_PIN J8 [get_ports SMA_MGT_REFCLK_P]

#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK0_N]
#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK0_P]
#set_property PACKAGE_PIN  [get_ports MGT_REFCLK1_P]
#set_property PACKAGE_PIN  [get_ports MGT_REFCLK1_N]
#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK1_N]
#set_property CLOCK_BUFFER_TYPE NONE [get_ports MGT_REFCLK1_P]

#set_property PACKAGE_PIN K6 [get_ports {MGT_RX_P[0]}]
#set_property PACKAGE_PIN K5 [get_ports {MGT_RX_N[0]}]
#set_property PACKAGE_PIN  [get_ports {MGT_RX_P[1]}]
#set_property PACKAGE_PIN  [get_ports {MGT_RX_N[1]}]
#set_property PACKAGE_PIN  [get_ports {MGT_RX_P[2]}]
#set_property PACKAGE_PIN  [get_ports {MGT_RX_N[2]}]
set_property PACKAGE_PIN K6 [get_ports {MGT_RX_P[3]}]
set_property PACKAGE_PIN K5 [get_ports {MGT_RX_N[3]}]

#set_property IOSTANDARD LVDS [get_ports RX_INIT_CLK_*]
#set_property DIFF_TERM false [get_ports RX_INIT_CLK_*]
#set_property PACKAGE_PIN  [get_ports RX_INIT_CLK_P]
#set_property PACKAGE_PIN  [get_ports RX_INIT_CLK_N]


# Debug LEDs
set_property PACKAGE_PIN AB8 [get_ports {LED[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[0]}]
set_property PACKAGE_PIN AA8 [get_ports {LED[1]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[1]}]
set_property PACKAGE_PIN AC9 [get_ports {LED[2]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[2]}]
set_property PACKAGE_PIN AB9 [get_ports {LED[3]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[3]}]
set_property PACKAGE_PIN AE26 [get_ports {LED[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[4]}]
set_property PACKAGE_PIN G19 [get_ports {LED[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[5]}]
set_property PACKAGE_PIN E18 [get_ports {LED[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[6]}]
set_property PACKAGE_PIN F16 [get_ports {LED[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[7]}]
set_property SLEW SLOW [get_ports LED*]


# User SMA: CMD encoder. Bank ...
set_property PACKAGE_PIN Y23 [get_ports USER_SMA_P]
set_property PACKAGE_PIN Y24 [get_ports USER_SMA_N]
set_property IOSTANDARD LVDS_25 [get_ports USER_SMA*]
#set_property IOSTANDARD LVCMOS25 [get_ports USER_SMA*]

# I2C pins
# Bus switch TI PCA9548 at address 0x74
# Switch address 0: Si570 clock
set_property PACKAGE_PIN K21 [get_ports I2C_SCL]
set_property PACKAGE_PIN L21 [get_ports I2C_SDA]
set_property IOSTANDARD LVCMOS25 [get_ports I2C_*]
set_property SLEW SLOW [get_ports I2C_*]


# Fan
set_property PACKAGE_PIN L26 [get_ports SM_FAN_PWM]
set_property IOSTANDARD LVCMOS25 [get_ports SM_FAN_PWM]
#set_property PACKAGE_PIN U22 [get_ports SM_FAN_TACH]
#set_property IOSTANDARD LVCMOS25 [get_ports SM_FAN_TACH]


#XADC
#set_property IOSTANDARD LVDS_25 [get_ports XADC_GPIO_01_N]
#set_property PACKAGE_PIN AA25 [get_ports XADC_GPIO_01_P]
#set_property PACKAGE_PIN AB25 [get_ports XADC_GPIO_01_N]
#set_property IOSTANDARD LVDS_25 [get_ports XADC_GPIO_01_P]
#set_property PACKAGE_PIN AA27 [get_ports XADC_GPIO_23_P]
#set_property IOSTANDARD LVDS_25 [get_ports XADC_GPIO_23_P]
#set_property PACKAGE_PIN AB28 [get_ports XADC_GPIO_23_N]
#set_property IOSTANDARD LVDS_25 [get_ports XADC_GPIO_23_N]
#in case we want to use the gpios as single ended signals
#set_property IOSTANDARD LCMOS25 [get_ports XADC_GPIO_23_P]


# Aurora IP core
set_false_path -to [get_pins -hier *aurora_64b66b_1lane_kc705_cdc_to*/D]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *data_sync_reg1}]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ *ack_sync_reg1}]
set_property LOC GTXE2_CHANNEL_X0Y8 [get_cells  aurora_64b66b_1lane_kc705_block_i/aurora_64b66b_1lane_kc705_i/inst/aurora_64b66b_1lane_kc705_wrapper_i/aurora_64b66b_1lane_kc705_multi_gt_i/aurora_64b66b_1lane_kc705_gtx_inst/gtxe2_i]




set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 33 [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
