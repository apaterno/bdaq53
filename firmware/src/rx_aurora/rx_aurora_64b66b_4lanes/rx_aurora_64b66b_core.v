/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none

`include "rx_aurora_64b66b_4lanes_kintex/exdes/aurora_64b66b_4lanes_exdes.v"

module rx_aurora_64b66b_core
#(
    parameter ABUSWIDTH = 16,
    parameter IDENTYFIER = 0,
    parameter AURORA_LANES = 4
)(
    input wire [3:0] RXP, RXN,
    output reg RX_CLK,
    input wire RX_CLK_IN_P, RX_CLK_IN_N,
    input wire INIT_CLK_IN_P, INIT_CLK_IN_N,

    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,

    input wire USERK_FIFO_READ,
    output wire USERK_FIFO_EMPTY,
    output wire [31:0] USERK_FIFO_DATA,

    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    input wire [7:0] BUS_DATA_IN,
    output reg [7:0] BUS_DATA_OUT,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,

    output wire RX_READY,
    output wire LOST_ERROR,

    input wire AURORA_RESET
);

localparam VERSION = 1;

// output format #ID (as parameter IDENTIFIER + 1 frame start + 16 bit data)

wire SOFT_RST;
assign SOFT_RST = (BUS_ADD==0 && BUS_WR);

wire RST;
assign RST = BUS_RST | SOFT_RST;

reg CONF_EN;

always @(posedge BUS_CLK) begin
    if(RST) begin
        CONF_EN <= 0;
    end
    else if(BUS_WR) begin
        if(BUS_ADD == 2)
            CONF_EN <= BUS_DATA_IN[0];
    end
end

reg [7:0] LOST_DATA_CNT;

wire RX_HARD_ERROR, SOFT_ERROR, PLL_LOCKED, RX_LANE_UP, RX_CHANNEL_UP;

always @(posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT <= VERSION;
        else if(BUS_ADD == 2)
            if (PLL_LOCKED)
                BUS_DATA_OUT <= {4'b0, PLL_LOCKED, RX_LANE_UP, RX_READY, CONF_EN};
            else
                BUS_DATA_OUT <= 8'd0;
        else if(BUS_ADD == 3)
            BUS_DATA_OUT <= LOST_DATA_CNT;
        else if(BUS_ADD == 4)
            BUS_DATA_OUT <= RX_LANE_UP;
    else
        BUS_DATA_OUT <= 8'b0;
    end
end

wire RST_SYNC;
wire RST_SOFT_SYNC;
reg  reset_rx;

cdc_reset_sync rst_pulse_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(RX_CLK), .pulse_out(RST_SOFT_SYNC));
assign RST_SYNC = RST_SOFT_SYNC;

wire CONF_EN_SYNC;
assign CONF_EN_SYNC  = CONF_EN;

wire USER_CLK;
wire INIT_CLK_P, INIT_CLK_N;
wire RX_CLK_P, RX_CLK_N;

// ---- aurora core for simulation -----//
parameter   CLOCKPERIOD_1 = 15.640;//12.5;

reg     gsr_r;
reg     gts_r;
reg     pma_init_r;
reg     gsr_done;            //Indicates the deassertion of GSR

`ifdef SYNTHESIS
    assign RX_CLK_P = RX_CLK_IN_P;
    assign RX_CLK_N = RX_CLK_IN_N;
    assign INIT_CLK_P = INIT_CLK_IN_P;
    assign INIT_CLK_N = INIT_CLK_IN_N;
`else
    assign  glbl.GSR = gsr_r;
    assign  glbl.GTS = gts_r;

    assign RX_CLK_P = RX_CLK;
    assign RX_CLK_N = ~RX_CLK;
    assign INIT_CLK_P = RX_CLK;
    assign INIT_CLK_N = ~RX_CLK;

    initial
        begin
         gts_r      = 1'b0;
         gsr_r      = 1'b1;
         gsr_done   = 1'b0;
         reset_rx   = 1'b1;
         #(130*CLOCKPERIOD_1);
         pma_init_r = 1'b1;
         gsr_r      = 1'b0;
         #(1600*CLOCKPERIOD_1);
         gsr_done   = 1'b1;
         pma_init_r = 1'b0;
         #(130*CLOCKPERIOD_1);
         reset_rx    = 1'b0;
    end

    initial
        RX_CLK = 1'b0;

    always
        #(CLOCKPERIOD_1 / 2) RX_CLK = !RX_CLK;
`endif

// AXI STREAM INTERFACE
wire RX_TLAST;
wire RX_TVALID;
wire [64*AURORA_LANES-1:0] RX_TDATA;
wire [8*AURORA_LANES-1:0]  RX_TKEEP;
wire [7:0] USER_K_ERR;
wire [64*AURORA_LANES-1:0] USER_K_DATA;
wire USER_K_VALID;


// ---- aurora core for simulation -----//
assign RX_READY = RX_CHANNEL_UP & RX_LANE_UP;

wire RST_USER_SYNC;
cdc_reset_sync rst_pulse_user_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(USER_CLK), .pulse_out(RST_USER_SYNC));

reg RX_TFIRST;
always@(posedge USER_CLK)
    if(RST_USER_SYNC)
        RX_TFIRST <= 1;
    else if(RX_TVALID & RX_TLAST)
        RX_TFIRST <= 1;
    else if(RX_TVALID)
        RX_TFIRST <= 0;

// Generate pulse on rising edge of USERK_RX_TFIRST
reg USER_K_VALID_delayed;
reg USERK_RX_TFIRST;
wire USERK_RX_TFIRST_COMB;
assign USERK_RX_TFIRST_COMB = USER_K_VALID & !USER_K_VALID_delayed;
always@(posedge USER_CLK) begin
    if(RST_USER_SYNC)
        USERK_RX_TFIRST <= 0;
    else if(USER_K_VALID & !USER_K_VALID_delayed)
        USERK_RX_TFIRST <= 1;
    else
        USERK_RX_TFIRST <= 0;
    USER_K_VALID_delayed <= USER_K_VALID;
end

localparam count_ones_logwidth = $clog2(8*AURORA_LANES);    //requires Verilog 2005
reg [count_ones_logwidth:0] count_ones;
integer idx;

always @* begin
  count_ones = {8*AURORA_LANES{1'b0}};
  for( idx = 0; idx<8*AURORA_LANES; idx = idx + 1) begin
    count_ones = count_ones + RX_TKEEP[idx];
  end
end


localparam RX_TFIRST_OFFSET = 64*AURORA_LANES;    //(DATA_SIZE_FIFO-1)-count_ones_logwidth;
wire [7:0] bytemask;
wire [count_ones_logwidth-2:0] bytemask_bytes;
assign bytemask_bytes = count_ones[count_ones_logwidth:2];

// CDC for DATA
localparam DATA_SIZE_FIFO = (count_ones_logwidth-1)+1+(64*AURORA_LANES);

reg [count_ones_logwidth-1:0] byte_cnt, byte_cnt_prev, bytestoread, bytestoread_prev;           // pointers for fifo access
wire [DATA_SIZE_FIFO-1:0] data_to_cdc, cdc_data_out, cdc_data_out_val, cdc_data_out_buffer;
wire cdc_wfull, cdc_fifo_empty, read_fifo_cdc;
wire [16:0] data_out;
wire [16:0] fifo_data_out_byte [(4*AURORA_LANES-1):0];

assign data_to_cdc = {bytemask_bytes, RX_TFIRST, RX_TDATA[(64*AURORA_LANES-1):0]};
assign read_fifo_cdc = !cdc_fifo_empty && (bytestoread==0 && bytestoread_prev!=0); //(byte_cnt_prev==1 & byte_cnt==0);//(byte_cnt_prev==0 & byte_cnt==1);                    // write at rising edge of each fifo pointer increment
assign data_out = {cdc_data_out[RX_TFIRST_OFFSET] && byte_cnt==1 ,fifo_data_out_byte[byte_cnt_prev & 4'hf][15:0]};

// CDC FIFO between Aurora- and BUS clock domains
cdc_syncfifo #(.DSIZE(DATA_SIZE_FIFO), .ASIZE(16)) cdc_syncfifo_i
(
    .rdata(cdc_data_out),
    .wfull(cdc_wfull),
    .rempty(cdc_fifo_empty),
    .wdata(data_to_cdc),
    .winc(RX_TVALID), .wclk(USER_CLK), .wrst(RST_USER_SYNC),
    .rinc(read_fifo_cdc), .rclk(BUS_CLK), .rrst(RST)
    );


// CDC for USER K
localparam DATA_USERK_SIZE_FIFO = 1+(64*AURORA_LANES);

reg [count_ones_logwidth-1:0] userk_byte_cnt, userk_byte_cnt_prev, userk_bytestoread, userk_bytestoread_prev;           // pointers for fifo access
wire [DATA_USERK_SIZE_FIFO-1:0] userk_data_to_cdc, userk_cdc_data_out;
wire userk_wfull, userk_cdc_fifo_empty, userk_read_fifo_cdc;
wire [16:0] userk_data_out;
wire [16:0] userk_fifo_data_out_byte [(4*AURORA_LANES-1):0];

assign userk_data_to_cdc = {0'b0, USER_K_DATA}; //USERK_RX_TFIRST
assign userk_read_fifo_cdc = !userk_cdc_fifo_empty && (userk_bytestoread==0 && userk_bytestoread_prev!=0);
assign userk_data_out = {userk_cdc_data_out[RX_TFIRST_OFFSET] && userk_byte_cnt==0 ,userk_fifo_data_out_byte[userk_byte_cnt_prev & 4'hf][15:0]};

cdc_syncfifo #(.DSIZE(DATA_USERK_SIZE_FIFO), .ASIZE(4)) userk_cdc_syncfifo_i
(
    .rdata(userk_cdc_data_out),
    .wfull(userk_wfull),
    .rempty(userk_cdc_fifo_empty),
    .wdata(userk_data_to_cdc),
    .winc(USERK_RX_TFIRST_COMB), .wclk(USER_CLK), .wrst(RST_USER_SYNC),
    .rinc(userk_read_fifo_cdc), .rclk(BUS_CLK), .rrst(RST)
    );


// Generic FIFO for handling RX bursts
wire write_out_fifo, fifo_full;
wire [23:0] cdc_data;
reg [23:0] cdc_data_delayed;
assign cdc_data = {7'b0, data_out};
assign write_out_fifo = byte_cnt_prev != 0;//(byte_cnt != 0 || byte_cnt_prev != 0);

always@(posedge BUS_CLK) begin
    cdc_data_delayed <= cdc_data;
end

gerneric_fifo #(.DATA_SIZE(24), .DEPTH(1024))  fifo_i
(   .clk(BUS_CLK), .reset(RST),
    .write(write_out_fifo),
    .read(FIFO_READ),
    .data_in(cdc_data_delayed),
    .full(fifo_full),
    .empty(FIFO_EMPTY),
    .data_out(FIFO_DATA[23:0]), .size()
    );


// Generic FIFO for handling USERK bursts
wire userk_write_out_fifo, userk_fifo_full;
wire [23:0] userk_cdc_data;
reg  [23:0] userk_cdc_data_delayed;
assign userk_cdc_data = {7'b0, userk_data_out};
assign userk_write_out_fifo = userk_byte_cnt_prev != 0;

always@(posedge BUS_CLK) begin
    userk_cdc_data_delayed <= userk_cdc_data;
end

gerneric_fifo #(.DATA_SIZE(24), .DEPTH(128))  userk_fifo_i
(   .clk(BUS_CLK), .reset(RST),
    .write(userk_write_out_fifo),
    .read(USERK_FIFO_READ),
    .data_in(userk_cdc_data_delayed),
    .full(userk_fifo_full),
    .empty(USERK_FIFO_EMPTY),
    .data_out(USERK_FIFO_DATA[23:0]),
    .size()
);

// Aurora receiver
aurora_64b66b_4lanes_exdes  #( .SIMPLEX_TIMER_VALUE(10) ) aurora_frame (
    // Error signals from Aurora
    .RX_HARD_ERR(RX_HARD_ERROR),
    .RX_SOFT_ERR(SOFT_ERROR),
    .RX_LANE_UP(RX_LANE_UP),
    .RX_CHANNEL_UP(RX_CHANNEL_UP),
    .PLL_LOCKED(PLL_LOCKED),

    .INIT_CLK_P(INIT_CLK_P),
    .INIT_CLK_N(INIT_CLK_N),

    .PMA_INIT(pma_init_r),

    .GTXQ0_P(RX_CLK_P),
    .GTXQ0_N(RX_CLK_N),

    .RXP(RXP),
    .RXN(RXN),

    // Error signals from the Local Link packet checker
    .DATA_ERR_COUNT(),

    //USER_K
    .USER_K_ERR(USER_K_ERR),
    .USER_K_DATA(USER_K_DATA),
    .USER_K_VALID(USER_K_VALID),

    // User IO
    .RESET(reset_rx), //RST_USER_SYNC

    .DRP_CLK_IN(USER_CLK),
    .USER_CLK(USER_CLK),
    .RX_TDATA(RX_TDATA),
    .RX_TVALID(RX_TVALID),
    .RX_TKEEP(RX_TKEEP),
    .RX_TLAST(RX_TLAST)
);


// FIFO access pointer: RX  DATA
always@(posedge BUS_CLK)
    if(RST) begin
        byte_cnt <= 0;
        bytestoread <= 0;
        bytestoread_prev <= 0;
    end
    else if(!cdc_fifo_empty && !fifo_full && bytestoread_prev == 0 )
    begin
        bytestoread <= {cdc_data_out[DATA_SIZE_FIFO-1:DATA_SIZE_FIFO-(count_ones_logwidth-1)], 1'b0};     //"Left shift" by 1, because we count in "half-words"
        byte_cnt <= byte_cnt + 1;
    end
    else if (!fifo_full & byte_cnt < bytestoread)
        byte_cnt <= byte_cnt + 1;
    else begin
        byte_cnt <= 0;
        bytestoread <= 0;
    end


// FIFO access pointer: USER DATA
always@(posedge BUS_CLK)
    if(RST) begin
        //userk_byte2_cnt <= 0;
        userk_bytestoread <= 0;
        userk_bytestoread_prev <= 0;
    end
    else if(!userk_cdc_fifo_empty && !userk_fifo_full && userk_bytestoread_prev == 0 )
    begin
        userk_bytestoread <= 4*AURORA_LANES;
        userk_byte_cnt <= userk_byte_cnt + 1;
        end
    else if (!userk_fifo_full & userk_byte_cnt < userk_bytestoread)
        userk_byte_cnt <= userk_byte_cnt + 1;
    else begin
        userk_byte_cnt <= 0;
        userk_bytestoread <= 0;
    end


// Resorting of received data frames
genvar aurora_ch;
generate
    for (aurora_ch=0; aurora_ch<AURORA_LANES; aurora_ch+=1) begin
        assign fifo_data_out_byte[(4*aurora_ch)+0] = {1'b0, cdc_data_out[ aurora_ch*64+31 : aurora_ch*64+16 ]};
        assign fifo_data_out_byte[(4*aurora_ch)+1] = {1'b0, cdc_data_out[ aurora_ch*64+15 : aurora_ch*64    ]};
        assign fifo_data_out_byte[(4*aurora_ch)+2] = {1'b0, cdc_data_out[ aurora_ch*64+63 : aurora_ch*64+48 ]};
        assign fifo_data_out_byte[(4*aurora_ch)+3] = {1'b0, cdc_data_out[ aurora_ch*64+47 : aurora_ch*64+32 ]};
/*
        // Restore original KWord, altered before by the xilinx rx core
        reg [7:0] userk_type;
        always@(*) begin
            case (userk_cdc_data_out_temp[ aurora_ch*64+7 : aurora_ch*64 ])
                8'h0 : userk_type = 8'hD2; //KWordMM
                8'h1 : userk_type = 8'h99; //KWordMA
                8'h2 : userk_type = 8'h55; //KWordAM
                8'h3 : userk_type = 8'hB4; //KWordAA
                8'h4 : userk_type = 8'hCC; //KWordEE
                default : userk_type = 8'h00;
            endcase
        end
        assign userk_cdc_data_out ={userk_type, userk_cdc_data_out_temp[aurora_ch*64+63 : aurora_ch*64+8]}; //userk_type
*/
        assign userk_fifo_data_out_byte[(4*(AURORA_LANES-1-aurora_ch))+0] = {1'b0, userk_cdc_data_out[ aurora_ch*64+31 : aurora_ch*64+16 ]};
        assign userk_fifo_data_out_byte[(4*(AURORA_LANES-1-aurora_ch))+1] = {1'b0, userk_cdc_data_out[ aurora_ch*64+15 : aurora_ch*64    ]};
        assign userk_fifo_data_out_byte[(4*(AURORA_LANES-1-aurora_ch))+2] = {1'b0, userk_cdc_data_out[ aurora_ch*64+63 : aurora_ch*64+48 ]};
        assign userk_fifo_data_out_byte[(4*(AURORA_LANES-1-aurora_ch))+3] = {1'b0, userk_cdc_data_out[ aurora_ch*64+47 : aurora_ch*64+32 ]};
    end
endgenerate


// Add the USERK_RX_TFIRST bit
assign userk_fifo_data_out_byte[2*AURORA_LANES-1][16] = userk_cdc_data_out[64*AURORA_LANES];


always@(posedge BUS_CLK) begin
    byte_cnt_prev <= byte_cnt;
    bytestoread_prev <= bytestoread;
    userk_byte_cnt_prev <= userk_byte_cnt;
    userk_bytestoread_prev <= userk_bytestoread;
end

always@(posedge USER_CLK) begin
    if(RST_USER_SYNC)
        LOST_DATA_CNT <= 0;
    else if (cdc_wfull && RX_TVALID && LOST_DATA_CNT != -1)
        LOST_DATA_CNT <= LOST_DATA_CNT +1;
end

assign FIFO_DATA[31:24]  =  IDENTYFIER[7:0];
assign LOST_ERROR = LOST_DATA_CNT != 0;

endmodule
