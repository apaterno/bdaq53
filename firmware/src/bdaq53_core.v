/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute of Bonn University
 * ------------------------------------------------------------
 */

`timescale 1ns / 1ps
//`default_nettype wire

localparam VERSION = {8'd0, 8'd3};  // VERSION{1,3} for version 1.3

`include "cmd_rd53/cmd_rd53.v"
`include "cmd_rd53/cmd_rd53_core.v"

`ifdef AURORA_1LANE
    `include "rx_aurora/rx_aurora_64b66b_1lane/rx_aurora_64b66b.v"
    `include "rx_aurora/rx_aurora_64b66b_1lane/rx_aurora_64b66b_core.v"
`elsif AURORA_2LANE
    `include "rx_aurora/rx_aurora_64b66b_2lanes/rx_aurora_64b66b.v"
    `include "rx_aurora/rx_aurora_64b66b_2lanes/rx_aurora_64b66b_core.v"
`elsif AURORA_4LANE
    `include "rx_aurora/rx_aurora_64b66b_4lanes/rx_aurora_64b66b.v"
    `include "rx_aurora/rx_aurora_64b66b_4lanes/rx_aurora_64b66b_core.v"
`else
    `include "rx_aurora/rx_aurora_64b66b_1lane/rx_aurora_64b66b.v"
    `include "rx_aurora/rx_aurora_64b66b_1lane/rx_aurora_64b66b_core.v"
`endif

`include "i2c/i2c.v"
`include "i2c/i2c_core.v"

`include "gpio/gpio.v"

`include "utils/cdc_pulse_sync.v"
`include "utils/cdc_reset_sync.v"
`include "utils/cdc_syncfifo.v"
`include "utils/generic_fifo.v"

module bdaq53_core(

    input wire          BUS_CLK,
    input wire          BUS_RST,
    input wire  [31:0]  BUS_ADD,
    inout wire  [31:0]  BUS_DATA,
    input wire          BUS_RD,
    input wire          BUS_WR,
    output wire         BUS_BYTE_ACCESS,

    // Clocks from oscillators and mux select
    input wire  CLK200_P, CLK200_N,
    input wire  RX_CLK_IN_P, RX_CLK_IN_N,
    output wire CLK_SEL,
    output wire REFCLK1_OUT,
    output wire TX_OUT_CLK,

    // PLL
    input wire  CLK_CMD,

    // Aurora lanes
    input wire [3:0] MGT_RX_P, MGT_RX_N,

    // CMD encoder
    input wire EXT_TRIGGER,
    output wire CMD_DATA, CMD_P, CMD_N, CMD_WRITING,

    // Displayport control signals
    output wire [3:0] GPIO_RESET,
    input wire [3:0]  GPIO_SENSE,

    // Debug signals
    output wire DEBUG_TX0, DEBUG_TX1,    //debug signal copy of CMD

    output wire RX_LANE_UP,
    output wire RX_CHANNEL_UP,
    output wire PLL_LOCKED,

    // I2C bus
    inout wire I2C_SDA,
    inout wire I2C_SCL,

    // FIFO cpontrol signals (TX FIFO of DAQ)
    output wire [31:0] FIFO_DATA,
    output wire FIFO_WRITE,
    input wire FIFO_EMPTY,
    input wire FIFO_FULL,

    output wire BX_CLK_EN,
    output wire DUT_RESET,
    output wire RESET_TB,
    input wire AURORA_RESET

/*
    // DDR3 Memory Interface
    output wire [14:0]ddr3_addr,
    output wire [2:0] ddr3_ba,
    output wire       ddr3_cas_n,
    output wire       ddr3_ras_n,
    output wire       ddr3_ck_n, ddr3_ck_p,
    output wire [0:0] ddr3_cke,
    output wire       ddr3_reset_n,
    inout  wire [7:0] ddr3_dq,
    inout  wire       ddr3_dqs_n, ddr3_dqs_p,
    output wire [0:0] ddr3_dm,
    output wire       ddr3_we_n,
    output wire [0:0] ddr3_cs_n,
    output wire [0:0] ddr3_odt,
*/
);


`ifdef COCOTB_SIM
    localparam BOARD = 16'd0;
`elsif BDAQ53
    localparam BOARD = 16'd1;
`elsif KC705
    localparam BOARD = 16'd3;
`endif

// -------  MODULE ADREESSES  ------- //
localparam I2C_BASEADDR  = 32'h1000;
localparam I2C_HIGHADDR  = 32'h2000-1;

localparam GPIO_BASEADDR = 32'h2000;
localparam GPIO_HIGHADDR = 32'h2100-1;

localparam GPIO_RESET_SENSE_BASEADDR = 32'h2100;
localparam GPIO_RESET_SENSE_HIGHADDR = 32'h2200-1;

localparam AURORA_RX_BASEADDR = 32'h6000;
localparam AURORA_RX_HIGHADDR = 32'h7000-1;

localparam CMD_RD53_BASEADDR = 32'h9000;
localparam CMD_RD53_HIGHADDR = 32'ha000-1;


// VERSION/BOARD READBACK
reg [7:0] BUS_DATA_OUT_REG;
always @ (posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT_REG <= VERSION[7:0];
        else if(BUS_ADD == 1)
            BUS_DATA_OUT_REG <= VERSION[15:8];
        else if(BUS_ADD == 2)
            BUS_DATA_OUT_REG <= BOARD[7:0];
        else if(BUS_ADD == 3)
            BUS_DATA_OUT_REG <= BOARD[15:8];
    end
end

reg READ_VER;
always @ (posedge BUS_CLK)
    if(BUS_RD & BUS_ADD < 5)
        READ_VER <= 1;
    else
        READ_VER <= 0;

assign BUS_DATA[7:0] = READ_VER ? BUS_DATA_OUT_REG : 8'hzz;

// CLOCKS
wire INIT_CLK_IN_P, INIT_CLK_IN_N;  // 50...200 MHz
assign INIT_CLK_IN_P = CLK200_P;
assign INIT_CLK_IN_N = CLK200_N;


// -------  USER MODULES  ------- //
wire [7:0] GPIO_IO;
gpio #(
    .BASEADDR(GPIO_BASEADDR),
    .HIGHADDR(GPIO_HIGHADDR),
    .ABUSWIDTH(32),
    .IO_WIDTH(8),
    .IO_DIRECTION(8'hff)
) i_gpio_rx (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .IO(GPIO_IO)
);

assign DUT_RESET = GPIO_IO[0];
assign BX_CLK_EN = GPIO_IO[2];
assign RESET_TB  = GPIO_IO[6];


// GPIO module to access the chip reset and power sense signals
gpio #(
    .BASEADDR(GPIO_RESET_SENSE_BASEADDR),
    .HIGHADDR(GPIO_RESET_SENSE_HIGHADDR),
    .ABUSWIDTH(32),
    .IO_WIDTH(8),
    .IO_DIRECTION(8'hf0)
) i_gpio_control (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .IO({GPIO_RESET, GPIO_SENSE})
);

// ------ I²C module with clock generator ------ //

(* KEEP = "{TRUE}" *)
wire I2C_CLK;

clock_divider #(
    .DIVISOR(1600)
) i_clock_divisor_i2c (
    .CLK(BUS_CLK),
    .RESET(1'b0),
    .CE(),
    .CLOCK(I2C_CLK)
);


localparam I2C_MEM_BYTES = 32;

i2c #(
    .BASEADDR(I2C_BASEADDR),
    .HIGHADDR(I2C_HIGHADDR),
    .ABUSWIDTH(32),
    .MEM_BYTES(I2C_MEM_BYTES)
)  i_i2c (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .I2C_CLK(I2C_CLK),
    .I2C_SDA(I2C_SDA),
    .I2C_SCL(I2C_SCL)
);


// ----- Command encoder ----- //
wire EXT_START_PIN;
wire CMD_EN;
assign EXT_START_PIN = 0;

cmd_rd53 #(
    .BASEADDR(CMD_RD53_BASEADDR),
    .HIGHADDR(CMD_RD53_HIGHADDR),
    .ABUSWIDTH(32)
) i_cmd_rd53 (
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),

    .EXT_START_PIN(EXT_START_PIN),
    .EXT_TRIGGER(EXT_TRIGGER),

    .CMD_WRITING(CMD_WRITING),
    .CMD_CLK(CLK_CMD),
    .CMD_EN(CMD_EN),
    .CMD_SERIAL_OUT(CMD_DATA),

    .CMD_P(CMD_P), .CMD_N(CMD_N)
);


// ----- AURORA ----- //
wire AURORA_RX_FIFO_READ;
wire AURORA_RX_FIFO_EMPTY;
wire [31:0] AURORA_RX_FIFO_DATA;
wire AUR_LOST_ERR;

assign FIFO_DATA = AURORA_RX_FIFO_DATA;

rx_aurora_64b66b #(
    .BASEADDR(AURORA_RX_BASEADDR),
    .HIGHADDR(AURORA_RX_HIGHADDR),
    .ABUSWIDTH(32),
    .IDENTYFIER(0)
) i_aurora_rx (
    .RXP( { MGT_RX_P[0], MGT_RX_P[1], MGT_RX_P[2], MGT_RX_P[3] } ),     // lane reordering
    .RXN( { MGT_RX_N[0], MGT_RX_N[1], MGT_RX_N[2], MGT_RX_N[3] } ),     // lane reordering
    .RX_CLK_IN_P(RX_CLK_IN_P),
    .RX_CLK_IN_N(RX_CLK_IN_N),
    .INIT_CLK_IN_P(INIT_CLK_IN_P),
    .INIT_CLK_IN_N(INIT_CLK_IN_N),
    .REFCLK1_OUT(REFCLK1_OUT),

    .TX_OUT_CLK(TX_OUT_CLK),
    .MGT_REF_SEL(CLK_SEL),
    .AURORA_RESET(AURORA_RESET),
    .LOST_ERROR(AUR_LOST_ERR),
    .RX_LANE_UP(RX_LANE_UP),
    .RX_CHANNEL_UP(RX_CHANNEL_UP),
    .PLL_LOCKED(PLL_LOCKED),

    .FIFO_READ(AURORA_RX_FIFO_READ),
    .FIFO_EMPTY(AURORA_RX_FIFO_EMPTY),
    .FIFO_DATA(AURORA_RX_FIFO_DATA),

    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0]),
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR)
);

assign FIFO_WRITE          = !FIFO_FULL && !AURORA_RX_FIFO_EMPTY;
assign AURORA_RX_FIFO_READ = !FIFO_FULL;

// DEBUGGING PORTS
assign DEBUG_TX0  = TX_OUT_CLK; // RX_CLK_IN_P   TX_OUT_CLK  AURORA_RESET    //CLKCMD && CMD_DATA;    // ------------------------------------------------------------------> DEBUGGING
assign DEBUG_TX1 = REFCLK1_OUT;    //CMD_DATA;

endmodule
